/** @file Ready made column definitions for primitive data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")

let basePrimitiveField = {
  name: "basePrimitiveField",
  class: "primitive",
  method: "int",
  min: 0,
  max: 1,
}

let boolField = rePurpose(basePrimitiveField, {
  name: "boolField",
  method: "bool",
})

let dateField = rePurpose(basePrimitiveField, {
  name: "dateField",
  method: "date",
  min: "2000-01-01",
  max: "2040-01-01",
  transform: [val => val.toISOString()],
})

let floatField = rePurpose(basePrimitiveField, {
  name: "floatField",
  method: "float",
  min: 0,
  max: 100000,
})

let floatStartAt1Field = rePurpose(floatField, {
  name: "floatStartAt1Field",
  min: 1,
})

let futureField = rePurpose(basePrimitiveField, {
  name: "futureField",
  method: "future",
  ref: "2024-01-01",
  transform: [val => val.toISOString()],
})

let intField = rePurpose(basePrimitiveField, {
  name: "intField",
  method: "int",
  min: 0,
  max: 1000,
})

let intStartAt1Field = rePurpose(intField, { name: "intStartAt1Field", min: 1 })

let pastField = rePurpose(basePrimitiveField, {
  name: "pastField",
  method: "past",
  ref: "2018-01-01",
  transform: [val => val.toISOString()],
})

let percentField = rePurpose(basePrimitiveField, {
  name: "percentField",
  method: "percent",
  transform: [val => `${val}%`],
})

let recentField = rePurpose(basePrimitiveField, {
  name: "recentField",
  method: "recent",
  ref: "2019-01-01",
  transform: [val => val.toISOString()],
})

let soonField = rePurpose(basePrimitiveField, {
  name: "soonField",
  method: "soon",
  ref: "2022-01-01",
  transform: [val => val.toISOString()],
})

let timeField = rePurpose(basePrimitiveField, {
  name: "timeField",
  method: "time",
})

let uuidField = rePurpose(basePrimitiveField, {
  name: "uuidField",
  method: "uuid",
  template: "nnn-h3hh/nhnh",
})

module.exports = {
  basePrimitiveField: basePrimitiveField,
  boolField: boolField,
  dateField: dateField,
  floatField: floatField,
  floatStartAt1Field: floatStartAt1Field,
  futureField: futureField,
  intField: intField,
  intStartAt1Field: intStartAt1Field,
  pastField: pastField,
  percentField: percentField,
  recentField: recentField,
  soonField: soonField,
  timeField: timeField,
  uuidField: uuidField,
}
