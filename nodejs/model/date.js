/** @file Ready made column definitions for date data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")
const { dateField } = require("./primitive")

let dy = 24 * 3600 * 1000
let wk = 7 * dy
let mn = 4 * wk
let yr = 365 * dy

let refDate = new Date(Date.parse("2020-10-01 01:01:01:0000"))
let adjustDate = adj => new Date(refDate - adj)

let dateOfBirthField = rePurpose(dateField, {
  name: "dateOfBirthField",
  min: adjustDate(yr * 100),
  max: refDate,
  ref: refDate,
})

let dateOfBirthOapField = rePurpose(dateField, {
  name: "dateOfBirthOAPField",
  min: adjustDate(yr * 100),
  max: adjustDate(yr * 65),
  ref: refDate,
})

let dateOfBirthAdultField = rePurpose(dateField, {
  name: "dateOfBirthAdultField",
  min: adjustDate(yr * 100),
  max: adjustDate(yr * 18),
  ref: refDate,
})

let dateOfBirthParentField = rePurpose(dateField, {
  name: "dateOfBirthParentField",
  min: adjustDate(yr * 60),
  max: adjustDate(yr * 24),
  ref: refDate,
})

let dateOfBirthStudentField = rePurpose(dateField, {
  name: "dateOfBirthStudentField",
  min: adjustDate(yr * 24),
  max: adjustDate(yr * 18),
  ref: refDate,
})

let dateOfBirthTeenField = rePurpose(dateField, {
  name: "dateOfBirthTeenField",
  min: adjustDate(yr * 18),
  max: adjustDate(yr * 13),
  ref: refDate,
})

let dateOfBirthChildField = rePurpose(dateField, {
  name: "dateOfBirthChildField",
  min: adjustDate(yr * 12),
  max: adjustDate(yr * 5),
  ref: refDate,
})

let dateOfBirthToddlerField = rePurpose(dateField, {
  name: "dateOfBirthToddlerField",
  min: adjustDate(yr * 5),
  max: adjustDate(yr * 2),
  ref: refDate,
})

let dateOfBirthBabyField = rePurpose(dateField, {
  name: "dateOfBirthBabyField",
  min: adjustDate(yr * 2),
  max: adjustDate(wk),
  ref: refDate,
})

let dayOfWeekField = rePurpose(dateField, {
  name: "dayOfWeekField",
  ref: refDate,
  transform: [dt => dt.toLocaleString("default", { weekday: "long" })],
})

let dayOfWeekAbrevField = rePurpose(dateField, {
  name: "dayOfWeekAbrevField",
  ref: refDate,
  transform: [dt => dt.toLocaleString("default", { weekday: "short" })],
})

let monthOfYearField = rePurpose(dateField, {
  name: "monthOfYearField",
  ref: refDate,
  transform: [dt => dt.toLocaleString("default", { month: "long" })],
})

let monthOfYearAbrevField = rePurpose(dateField, {
  name: "monthOfYearAbrevField",
  ref: refDate,
  transform: [dt => dt.toLocaleString("default", { month: "short" })],
})

let lastWeekField = rePurpose(dateField, {
  name: "lastWeekField",
  min: adjustDate(wk),
  max: refDate,
  ref: refDate,
})

let lastMonthField = rePurpose(dateField, {
  name: "lastMonthField",
  min: adjustDate(mn),
  max: refDate,
  ref: refDate,
})

let last6MonthsField = rePurpose(dateField, {
  name: "last6MonthsField",
  min: adjustDate(mn * 6),
  max: refDate,
  ref: refDate,
})

let lastYearField = rePurpose(dateField, {
  name: "lastYearField",
  min: adjustDate(yr),
  max: refDate,
  ref: refDate,
})

let last2YearsField = rePurpose(dateField, {
  name: "last2YearsField",
  min: adjustDate(yr * 2),
  max: refDate,
})

let lastDecadeField = rePurpose(dateField, {
  name: "lastDecadeField",
  min: adjustDate(yr * 10),
  max: refDate,
  ref: refDate,
})

let lastCenturyField = rePurpose(dateField, {
  name: "lastCenturyField",
  min: adjustDate(yr * 130),
  max: refDate,
  ref: refDate,
})

let withinDaysField = rePurpose(dateField, {
  name: "withinDaysField",
  min: refDate,
  max: adjustDate(-1 * wk * 2),
  ref: refDate,
})

let withinWeeksField = rePurpose(dateField, {
  name: "withinWeeksField",
  min: refDate,
  max: adjustDate(-1 * wk * 6),
  ref: refDate,
})

let withinMonthsField = rePurpose(dateField, {
  name: "withinMonthsField",
  min: refDate,
  max: adjustDate(-1 * mn * 6),
  ref: refDate,
})

let within12MonthsField = rePurpose(dateField, {
  name: "within12MonthsField",
  min: refDate,
  max: adjustDate(-1 * yr),
  ref: refDate,
})

let withinYearsField = rePurpose(dateField, {
  name: "withinYearsField",
  min: adjustDate(-1 * yr * 2),
  max: adjustDate(-1 * yr * 15),
  ref: refDate,
})

let sciFiField = rePurpose(dateField, {
  name: "sciFiField",
  min: adjustDate(-1 * yr * 10),
  max: adjustDate(-1 * yr * 3000),
  ref: refDate,
})

module.exports = {
  dateOfBirthField: dateOfBirthField,
  dateOfBirthOapField: dateOfBirthOapField,
  dateOfBirthAdultField: dateOfBirthAdultField,
  dateOfBirthParentField: dateOfBirthParentField,
  dateOfBirthStudentField: dateOfBirthStudentField,
  dateOfBirthTeenField: dateOfBirthTeenField,
  dateOfBirthChildField: dateOfBirthChildField,
  dateOfBirthToddlerField: dateOfBirthToddlerField,
  dateOfBirthBabyField: dateOfBirthBabyField,
  dayOfWeekField: dayOfWeekField,
  dayOfWeekAbrevField: dayOfWeekAbrevField,
  monthOfYearField: monthOfYearField,
  monthOfYearAbrevField: monthOfYearAbrevField,
  lastWeekField: lastWeekField,
  lastMonthField: lastMonthField,
  last6MonthsField: last6MonthsField,
  lastYearField: lastYearField,
  last2YearsField: last2YearsField,
  lastDecadeField: lastDecadeField,
  lastCenturyField: lastCenturyField,
  withinDaysField: withinDaysField,
  withinWeeksField: withinWeeksField,
  withinMonthsField: withinMonthsField,
  within12MonthsField: within12MonthsField,
  withinYearsField: withinYearsField,
  sciFiField: sciFiField,
}
