/** @file Ready made column definitions for getting prepared data in files. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")

let baseRawField = { name: "baseRawField", class: "raw", file: "raw" }

let academicField = rePurpose(baseRawField, {
  name: "academicField",
  file: "academic",
})

let animalField = rePurpose(baseRawField, {
  name: "animalField",
  file: "animal",
})

let animalNameField = rePurpose(animalField, {
  name: "animalNameField",
  field: "animal",
})

let animalPrefixField = rePurpose(baseRawField, {
  name: "animalPrefixField",
  file: "animal_prefix",
})

let animalSuffixField = rePurpose(baseRawField, {
  name: "animalSuffixField",
  file: "animal_suffix",
})

let businessTypeField = rePurpose(baseRawField, {
  name: "businessTypeField",
  file: "business_type",
})

let carField = rePurpose(baseRawField, { name: "carField", file: "car" })

let charField = rePurpose(baseRawField, { name: "charField", file: "char" })

let colorField = rePurpose(baseRawField, {
  name: "colorField",
  file: "color",
})

let districtSuffixField = rePurpose(baseRawField, {
  name: "districtSuffixField",
  file: "district_suffix",
})

let geoLocationField = rePurpose(baseRawField, {
  name: "geoLocationField",
  file: "geo_location",
})

let jobTitleField = rePurpose(baseRawField, {
  name: "jobTitleField",
  file: "job_title",
})

let personField = rePurpose(baseRawField, {
  name: "personField",
  file: "person",
})

let firstNameField = rePurpose(personField, {
  name: "firstNameField",
  field: "firstName",
})

let productField = rePurpose(baseRawField, {
  name: "productField",
  file: "product",
})

let productNameField = rePurpose(productField, {
  name: "productNameField",
  field: "product",
})

let streetTypeField = rePurpose(baseRawField, {
  name: "streetTypeField",
  file: "street_type",
})

let lastNameField = rePurpose(baseRawField, {
  name: "lastNameField",
  file: "lastname",
})

let transportField = rePurpose(baseRawField, {
  name: "transportField",
  file: "transport",
})

let wordPrefixField = rePurpose(baseRawField, {
  name: "wordPrefixField",
  file: "word_prefix",
})

let wordSuffixField = rePurpose(baseRawField, {
  name: "wordSuffixField",
  file: "word_suffix",
})

module.exports = {
  baseRawField: baseRawField,
  academicField: academicField,
  animalField: animalField,
  animalNameField: animalNameField,
  animalPrefixField: animalPrefixField,
  animalSuffixField: animalSuffixField,
  businessTypeField: businessTypeField,
  carField: carField,
  charField: charField,
  colorField: colorField,
  districtSuffixField: districtSuffixField,
  firstNameField: firstNameField,
  geoLocationField: geoLocationField,
  jobTitleField: jobTitleField,
  personField: personField,
  firstNameField: firstNameField,
  productField: productField,
  productNameField: productNameField,
  streetTypeField: streetTypeField,
  lastNameField: lastNameField,
  transportField: transportField,
  wordPrefixField: wordPrefixField,
  wordSuffixField: wordSuffixField,
}
