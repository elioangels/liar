/** @file Ready made column definitions for time data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")
const { timeField } = require("./primitive")

let withinMilliSecondsField = rePurpose(timeField, {
  name: "withinMilliSecondsField",
  hours: { min: 0, max: 0 },
  mins: { min: 0, max: 0 },
  secs: { min: 0, max: 0 },
  milli: { min: 0, max: 1000 },
  format: "milli",
})

let withinSecondsField = rePurpose(timeField, {
  name: "withinSecondsField",
  hours: { min: 0, max: 0 },
  mins: { min: 0, max: 0 },
  secs: { min: 0, max: 59 },
  milli: { min: 0, max: 1000 },
  format: "milli",
})

let withinMinutesField = rePurpose(timeField, {
  name: "withinMinutesField",
  hours: { min: 0, max: 0 },
  mins: { min: 0, max: 59 },
  secs: { min: 0, max: 59 },
  milli: { min: 0, max: 0 },
})

let withinOpeningHoursField = rePurpose(timeField, {
  name: "openingHoursField",
  hours: { min: 8, max: 17 },
  mins: { min: 0, max: 59 },
  secs: { min: 0, max: 59 },
  milli: { min: 0, max: 0 },
})

module.exports = {
  withinMilliSecondsField: withinMilliSecondsField,
  withinSecondsField: withinSecondsField,
  withinMinutesField: withinMinutesField,
  withinOpeningHoursField: withinOpeningHoursField,
}
