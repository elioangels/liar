/** @file Ready made column definitions for location data. */
"use strict"
const ITransform = require("../liar/itransform")
const { rePurpose } = require("../liar/ijusthelp")
const { spaceField } = require("./common")
const {
  ainuWordField,
  latinWordField,
  englishWordField,
  compoundWordField,
} = require("./gossip")
const { of100Field } = require("./int")
const {
  districtSuffixField,
  geoLocationField,
  streetTypeField,
  lastNameField,
} = require("./raw")

let poshAddress1Field = {
  name: "poshAddress1Field",
  class: "concat",
  from: {
    pretentiousNumberSometimes: {
      class: "concat",
      from: {
        number: {
          class: "quick",
          list: "123456IVX".split(""),
        },
        spaceField,
      },
      transform: [ITransform.funky.splutter(50)],
    },
    wordTheSometimes: {
      class: "exact",
      value: "The ",
      transform: [ITransform.funky.splutter(50)],
    },
    pretentiousName: {
      class: "choose",
      between: {
        lastNameField,
        compoundWordField,
      },
    },
  },
  transform: [ITransform.title],
}

let address1Field = {
  name: "address1Field",
  class: "concat",
  from: {
    of100Field,
    spaceField,
    englishWordField,
    spaceField,
    streetTypeField,
  },
  transform: [ITransform.title],
}

let address2Field = {
  name: "address2Field",
  class: "concat",
  from: {
    ainuWordField,
    districtSuffixField,
  },
  transform: [ITransform.title],
}

let localityField = {
  name: "locality",
  class: "concat",
  from: {
    latinWordField,
    districtSuffixField,
  },
  transform: [ITransform.title],
}

let postCodeField = rePurpose(of100Field, {
  name: "postCodeField",
  min: 10000,
  max: 999999,
})

let variedAddress1Field = {
  name: "variedAddress1Field",
  class: "choose",
  between: {
    address1Field,
    poshAddress1Field,
  },
}

module.exports = {
  poshAddress1Field: poshAddress1Field,
  address1Field: address1Field,
  variedAddress1Field: variedAddress1Field,
  address2Field: address2Field,
  localityField: localityField,
  geoLocationField: geoLocationField,
  postCodeField: postCodeField,
}
