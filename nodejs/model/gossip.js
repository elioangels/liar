/** @file Ready made column definitions for textual data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")
const { wordPrefixField, wordSuffixField } = require("./raw")

let baseBlurbField = {
  name: "baseBlurbField",
  class: "gossip",
  method: "words",
  min: 1,
  max: 1,
  file: "ainu",
}

let englishDictField = rePurpose(baseBlurbField, {
  name: "englishDictField",
  file: "english",
})

let ainuWordField = rePurpose(baseBlurbField, {
  name: "ainuWordField",
  file: "ainu",
})
let danishWordField = rePurpose(baseBlurbField, {
  name: "danishWordField",
  file: "danish",
})

let englishWordField = rePurpose(englishDictField, {
  name: "englishWordField",
  field: "word",
})

let compoundWordField = {
  name: "compoundWordField",
  class: "concat",
  from: {
    prefix: rePurpose(wordPrefixField),
    word: rePurpose(englishWordField),
    suffix: rePurpose(wordSuffixField),
  },
}

let latinWordField = rePurpose(baseBlurbField, {
  name: "latinWordField",
  file: "latin",
})

let ainuTitleField = rePurpose(ainuWordField, {
  name: "ainuTitleField",
  method: "paragraph",
  min: 7,
  max: 10,
})

let danishTitleField = rePurpose(danishWordField, {
  name: "ainuTitleField",
  method: "paragraph",
  min: 7,
  max: 10,
})

let englishTitleField = rePurpose(englishWordField, {
  name: "englishTitleField",
  method: "paragraph",
  min: 7,
  max: 10,
})

let latinTitleField = rePurpose(latinWordField, {
  name: "latinTitleField",
  method: "paragraph",
  min: 7,
  max: 10,
})

let elioSin = {
  class: "gossip",
  method: "html",
  file: "ainu",
  html: {
    tags: ["h3", "h4"],
    max: 4,
    min: 2,
    below: {
      heavenHell: {
        tags: ["blockquote", "aside"],
        max: 5,
        min: 3,
      },
      pillar: {
        tags: ["p", "section"],
        max: 3,
        min: 1,
      },
      ul: {
        tags: ["ul", "ol"],
        max: 1,
        min: 1,
        nest: {
          li: {
            tags: ["li"],
            max: 6,
            min: 2,
          },
        },
      },
    },
  },
}

module.exports = {
  elioSin: elioSin,
  baseBlurbField: baseBlurbField,
  ainuWordField: ainuWordField,
  danishWordField: danishWordField,
  englishWordField: englishWordField,
  compoundWordField: compoundWordField,
  latinWordField: latinWordField,
  ainuTitleField: ainuTitleField,
  danishTitleField: danishTitleField,
  englishTitleField: englishTitleField,
  latinTitleField: latinTitleField,
}
