/** @file Ready made column definitions for personal details data. */
"use strict"
const ITransform = require("../liar/itransform")
const { rePurpose } = require("../liar/ijusthelp")
const { atSymbol, spaceField } = require("./common")
const { dateOfBirthField } = require("./date")
const {
  address1Field,
  address2Field,
  localityField,
  geoLocationField,
  postCodeField,
} = require("./location")
const { intField } = require("./primitive")
const { personField, streetTypeField, lastNameField } = require("./raw")

let titleField = rePurpose(personField, { name: "titleField", field: "title" })
let firstNameField = rePurpose(personField, {
  name: "firstNameField",
  field: "firstName",
})
let genderField = rePurpose(personField, { name: "genderField", field: "sex" })

let userNameField = {
  name: "userNameField",
  class: "concat",
  from: {
    nameOrInitial: {
      class: "choose",
      between: {
        firstName: { class: "field", field: "firstNameField" },
        firstInitial: {
          class: "concat",
          from: {
            char1FirstName: {
              class: "field",
              field: "firstNameField",
              transform: [ITransform.chomp],
            },
            lowerLastName: { class: "field", field: "lastNameField" },
          },
        },
      },
    },
    sep: {
      class: "quick",
      list: ["", "_", "-", "."],
      transform: [ITransform.funky.splutter(50)],
    },
    lastName: { class: "field", field: "lastNameField" },
  },
  transform: [ITransform.lower],
}

let personalDomainField = {
  name: "personalDomainField",
  class: "concat",
  from: {
    userName: { class: "field", field: "userNameField" },
    randomTLD: {
      class: "marak",
      mustache: ".{{internet.domainSuffix}}",
    },
  },
}

let personalEmailField = {
  name: "personalEmailField",
  class: "concat",
  from: {
    userNameField,
    atSymbol,
    personalDomain: { class: "field", field: "personalDomainField" },
  },
  transform: [ITransform.lower],
}

let personalWebField = {
  name: "personalWebField",
  class: "concat",
  from: {
    httpWWW: { class: "exact", value: "http://www." },
    personalDomain: { class: "field", field: "personalDomainField" },
  },
  transform: [ITransform.lower],
}

let personalPhoneField = {
  name: "personalPhoneField",
  class: "concat",
  from: {
    zero: { class: "exact", value: "0" },
    digit4: rePurpose(intField, { min: 1100, max: 1999 }),
    spaceField,
    digit6: rePurpose(intField, { min: 100000, max: 999999 }),
  },
}

let personalMobileField = {
  name: "personalMobileField",
  class: "concat",
  from: {
    zero: { class: "exact", value: "0" },
    digit4: rePurpose(intField, { min: 7100, max: 9999 }),
    spaceField,
    digit6: rePurpose(intField, { min: 100000, max: 999999 }),
  },
}

let homeAddress1Field = rePurpose(address1Field, { name: "homeAddress1Field" })
let homeAddress2Field = rePurpose(address2Field, { name: "homeAddress2Field" })
let homeLocalityField = rePurpose(localityField, { name: "homeLocalityField" })
let homeGeoLocationField = rePurpose(geoLocationField, {
  name: "homeGeoLocationField",
})
let homePostCodeField = rePurpose(postCodeField, { name: "homePostCodeField" })

let skinToneField = {
  name: "skinToneField",
  class: "quick",
  list: [
    "#FFDFC4",
    "#F0D5BE",
    "#EECEB3",
    "#E1B899",
    "#E5C298",
    "#FFDCB2",
    "#E5B887",
    "#E5A073",
    "#E79E6D",
    "#DB9065",
    "#CE967C",
    "#C67856",
    "#BA6C49",
    "#A57257",
    "#F0C8C9",
    "#DDA8A0",
    "#B97C6D",
    "#A8756C",
    "#AD6452",
    "#5C3836",
    "#CB8442",
    "#BD723C",
    "#704139",
    "#A3866A",
    "#870400",
    "#710101",
    "#430000",
    "#5B0001",
    "#302E2E",
    "#000000",
  ],
}

let hairColorField = {
  name: "hairColorField",
  class: "quick",
  list: [
    "#090806",
    "#2C222B",
    "#3B3024",
    "#4E433F",
    "#504444",
    "#533D32",
    "#554838",
    "#6A4E42",
    "#71635A",
    "#8D4A43",
    "#91553D",
    "#977961",
    "#A56B46",
    "#A7856A",
    "#B55239",
    "#B7A69E",
    "#B89778",
    "#CABFB1",
    "#D6C4C2",
    "#DCD0BA",
    "#DEBC99",
    "#E5C8A8",
    "#E6CEA8",
    "#FFF5E1",
  ],
}

let accessoryColorField = {
  name: "accessoryColorField",
  class: "quick",
  list: [
    "#FFB6C1",
    "#FFC0CB",
    "#DC143C",
    "#DB7093",
    "#FF69B4",
    "#FF1493",
    "#C71585",
    "#DA70D6",
    "#D8BFD8",
    "#DDA0DD",
    "#EE82EE",
    "#FF00FF",
    "#FF00FF",
    "#8B008B",
    "#800080",
    "#BA55D3",
    "#9400D3",
    "#9932CC",
    "#4B0082",
    "#8A2BE2",
    "#9370DB",
    "#7B68EE",
    "#6A5ACD",
    "#483D8B",
    "#E6E6FA",
    "#0000FF",
    "#0000CD",
    "#191970",
    "#00008B",
    "#000080",
    "#4169E1",
    "#6495ED",
    "#B0C4DE",
    "#778899",
    "#708090",
    "#1E90FF",
    "#4682B4",
    "#87CEFA",
    "#87CEEB",
    "#00BFFF",
    "#ADD8E6",
    "#B0E0E6",
    "#5F9EA0",
    "#AFEEEE",
    "#00FFFF",
    "#00FFFF",
    "#00CED1",
    "#2F4F4F",
    "#008B8B",
    "#008080",
    "#48D1CC",
    "#20B2AA",
    "#40E0D0",
    "#7FFFD4",
    "#66CDAA",
    "#00FA9A",
    "#00FF7F",
    "#3CB371",
    "#2E8B57",
    "#90EE90",
    "#98FB98",
    "#8FBC8F",
    "#32CD32",
    "#00FF00",
    "#228B22",
    "#008000",
    "#006400",
    "#7FFF00",
    "#7CFC00",
    "#ADFF2F",
    "#556B2F",
    "#9ACD32",
    "#6B8E23",
    "#F5F5DC",
    "#FAFAD2",
    "#808000",
    "#BDB76B",
    "#FFFACD",
    "#EEE8AA",
    "#F0E68C",
    "#FFD700",
    "#FFF8DC",
    "#DAA520",
    "#B8860B",
    "#FDF5E6",
    "#F5DEB3",
    "#FFE4B5",
    "#FFA500",
    "#FFEFD5",
    "#FFEBCD",
    "#FFDEAD",
    "#FAEBD7",
    "#D2B48C",
    "#DEB887",
    "#FFE4C4",
    "#FF8C00",
    "#FAF0E6",
    "#CD853F",
    "#FFDAB9",
    "#F4A460",
    "#D2691E",
    "#8B4513",
    "#A0522D",
    "#FFA07A",
    "#FF7F50",
    "#FF4500",
    "#E9967A",
    "#FF6347",
  ],
}

module.exports = {
  titleField: titleField,
  firstNameField: firstNameField,
  lastNameField: lastNameField,
  genderField: genderField,
  userNameField: userNameField,
  homeAddress1Field: homeAddress1Field,
  homeAddress2Field: homeAddress2Field,
  homeLocalityField: homeLocalityField,
  homeGeoLocationField: homeGeoLocationField,
  homePostCodeField: homePostCodeField,
  personalDomainField: personalDomainField,
  personalEmailField: personalEmailField,
  personalWebField: personalWebField,
  personalPhoneField: personalPhoneField,
  personalMobileField: personalMobileField,
  skinToneField: skinToneField,
  hairColorField: hairColorField,
  accessoryColorField: accessoryColorField,
}
