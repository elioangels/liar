/** @file Ready made column definitions for products and marketing data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")
const { ainuWordField, englishWordField } = require("./gossip")
const { floatField } = require("./primitive")
const { colorField, productNameField } = require("./raw")

let productWeightField = rePurpose(floatField, {
  name: "productWeightField",
  min: 0.3,
  max: 20,
})

let postageCostField = {
  name: "postageCostField",
  class: "field",
  field: "productWeightField",
  transform: [val => val * 10, val => val / 2, val => val + 0.99],
}

let productColorField = rePurpose(colorField, {
  name: "productColorField",
  field: ["color", "hex"],
})

let productStockedField = {
  name: "productStockedField",
  class: "toothpaste",
  list: ["In stock", "No stock", "At supplier", "Discontinued"],
}

let tweetField = rePurpose(englishWordField, {
  name: "tweetField",
  method: "paragraph",
  min: 6,
  max: 15,
})

let descriptionField = rePurpose(ainuWordField, {
  name: "descriptionField",
  method: "html",
  p: {
    tags: ["p"],
    max: 3,
    min: 1,
  },
})

let webPageField = {
  name: "webPageField",
  class: "concat",
  from: {
    productNameField: {
      class: "concat",
      from: {
        h1Open: {
          class: "exact",
          value: "<h1>",
        },
        productNameField,
        h1Close: {
          class: "exact",
          value: "</h1>",
        },
      },
    },
    pageHTML: rePurpose(ainuWordField, {
      method: "html",
      h2: {
        tags: ["h2"],
        max: 3,
        min: 2,
        below: {
          h3: {
            tags: ["h3"],
            max: 7,
            min: 3,
            below: {
              blockquote: {
                tags: ["blockquote", "aside"],
                max: 1,
                min: 1,
              },
              p: {
                tags: ["p"],
                max: 3,
                min: 1,
              },
              ul: {
                tags: ["ul", "ol"],
                max: 1,
                min: 1,
                nest: {
                  li: {
                    tags: ["li"],
                    max: 6,
                    min: 2,
                  },
                },
              },
            },
          },
        },
      },
    }),
  },
}

module.exports = {
  productNameField: productNameField,
  productColorField: productColorField,
  postageCostField: postageCostField,
  productStockedField: productStockedField,
  productWeightField: productWeightField,
  tweetField: tweetField,
  descriptionField: descriptionField,
  webPageField: webPageField,
}
