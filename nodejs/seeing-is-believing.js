"use strict"
const ITransform = require("./liar/itransform")
const Pinocchio = require("./pinocchio")
const { rePurpose } = require("./liar/ijusthelp")

let numberOfRows = 20
let pinocchio = new Pinocchio(numberOfRows)

let incantation = {
  class: "concat",
  from: {
    eng: {
      class: "gossip",
      file: "english",
      method: "words",
      field: ["word"],
      min: 1,
      max: 1,
    },
    ainu: {
      class: "gossip",
      file: "ainu",
      method: "words",
      min: 1,
      max: 1,
    },
    lat: {
      class: "gossip",
      file: "latin",
      method: "words",
      min: 1,
      max: 1,
    },
    emphatically: {
      class: "quick",
      list: ["!", "!!", "!!!"],
    },
  },
  transform: [ITransform.spaceless, ITransform.title],
}

let wizard = {
  class: "toothpaste",
  list: ["Voldemort", "Harry Potter"],
}

let lies = pinocchio.lies({
  wizard: wizard,
  incantation: incantation,
})

console.log(lies)
