/** @file Creates realistic, random textual data */
"use strict"
const _ = require("lodash")
const faker = require("faker")

const IAmPrimitive = require("./iamprimitive")
const IJustHelp = require("./ijusthelp")

/**
 * @file Creates an array of words and punctuation.
 * @param {Array} wordList in the chosen language.
 * @param {Object} opts to control the output. // { min, max, punctuate, }
 * @returns {Array} of words.
 */
const wordly = (wordList, opts) => {
  let wordlyOpts = IJustHelp.rePurpose(opts)
  if (!wordlyOpts) wordlyOpts = {}
  if (!wordlyOpts.min) wordlyOpts.min = 1
  if (!wordlyOpts.max) wordlyOpts.max = 20
  if (!wordlyOpts.punctuate) wordlyOpts.punctuate = false
  // Random size.
  let size = IAmPrimitive.int(wordlyOpts)
  // wordList too short
  while (wordList.length < size) {
    wordList = wordList.concat(wordList)
  }
  // Random startPoint in the wordList.
  let startPoint = IAmPrimitive.int({
    min: 1,
    max: wordList.length - size,
  })
  // Shuffle.
  wordList = faker.helpers.shuffle([...wordList])
  // Slice words out of the wordList.
  let blurb = wordList.slice(startPoint, startPoint + size)
  // Insert punctuation.
  if (wordlyOpts.punctuate) {
    blurb = IJustHelp.puncture(blurb, wordlyOpts)
  }
  // Return as string.
  return blurb
}

/**
 * @file Creates realistic looking sentence.
 * @param {Array} wordList in the chosen language.
 * @param {Object} opts to control the output. // { min, max, punctuate, }
 * @returns {string} of words.
 */
const words = (wordList, opts) => {
  let wordsOpts = IJustHelp.rePurpose(opts)
  return wordly(wordList, wordsOpts)
    .join(" ")
    .replace(/ [\.,;:]/g, c => c.trim())
}

/**
 * @file Creates an array of words and punctuation.
 * @param {Array} wordList in the chosen language.
 * @param {Object} opts to control the output. // { min, max, punctuate, }
 * @returns {Array} of words terminated by a fullstop.
 */
const paragraphly = (wordList, opts) => {
  let paragraphlyOpts = IJustHelp.rePurpose(opts)
  if (!paragraphlyOpts) paragraphlyOpts = {}
  if (!paragraphlyOpts.min) paragraphlyOpts.min = 20
  if (!paragraphlyOpts.max) paragraphlyOpts.max = 200
  if (!paragraphlyOpts.punctuate)
    paragraphlyOpts.punctuate = ",,,,...?!:".split("")
  if (!paragraphlyOpts.densage) paragraphlyOpts.densage = 50
  let blurb = wordly(wordList, paragraphlyOpts)
  // Add a fullstop.
  blurb.push(".")
  // Capitalize the first word.
  blurb.splice(0, 1, _.capitalize(_.nth(blurb, 0)))
  return blurb
}

/**
 * @file Creates a realistic looking single paragraph.
 * @param {Array} wordList in the chosen language.
 * @param {Object} opts to control the output. // { min, max, punctuate, }
 * @returns {string} of words terminated by a fullstop.
 */
const paragraph = (wordList, opts) => {
  let paragraphOpts = IJustHelp.rePurpose(opts)
  return paragraphly(wordList, paragraphOpts)
    .join(" ")
    .replace(/ [\.\?!,;:]/g, c => c.trim())
}

/**
 * @file Creates realistic looking paragraphs.
 * @param {Array} wordList in the chosen language.
 * @param {Object} opts to control the output. // { min, max, punctuate, }
 * @returns {string} of paragraphs separated by line breaks.
 */
const paragraphs = (wordList, opts) => {
  let paragraphsOpts = IJustHelp.rePurpose(opts)
  paragraphsOpts.fuck = true
  if (!paragraphsOpts) paragraphsOpts = {}
  if (!paragraphsOpts.min) paragraphsOpts.min = 2
  if (!paragraphsOpts.max) paragraphsOpts.max = 20
  let size = IAmPrimitive.int(opts)
  // min and max are for number of paragraphs. Rely on randomness for size of
  // paragraphs.
  delete paragraphsOpts.min
  delete paragraphsOpts.max
  // Collect <size> of paragraphs and join with line breaks.
  return new Array(size)
    .fill()
    .map(_ => paragraph(wordList, paragraphsOpts))
    .join("\n\n")
}

const htmlNano = (wordList, tag) => {
  return (
    `<${tag}>` +
    paragraph(wordList, { min: 1, max: 3, densage: 30 }) +
    `</${tag}>`
  )
}

const htmlShort = (wordList, tag) => {
  return (
    `<${tag}>` +
    paragraph(wordList, { min: 3, max: 20, densage: 30 }) +
    `</${tag}>`
  )
}

const htmlLong = (wordList, tag) => {
  return (
    `<${tag}>` +
    paragraph(wordList, { min: 30, max: 200, densage: 30 }) +
    `</${tag}>`
  )
}

/** Generate realistic looking HTML.

* @param {Array} wordList in the chosen language.
* @param {Object} nest e.g.
* @returns {string} of html separated by line breaks.
{
  h1: {
    min: 1,
    max: 1,
    nest: {
      "h2,h3": {
        min: 2,
        max: 4,
        nest: {
          "p,ul,ol,h3,h4": { min: 3, max: 6 }
        }
      }
    }
  }
}
*/
const nanoTags = ["h1", "dt", "td", "th"]
const shortTags = ["h2", "h3", "h4", "li", "blockquote"]
const nestedTags = [
  "table",
  "thead",
  "tbody",
  "tr",
  "nav",
  "menu",
  "dl",
  "ul",
  "ol",
]
const html = (wordList, opts) => {
  let htmlOpts = IJustHelp.rePurpose(opts)
  let blurb = []
  // For each block of tags.
  for (let [key, tagOpts] of Object.entries(htmlOpts)) {
    // Repeat this tag the given number of times (controlled by `min` and `max`)
    let size = IAmPrimitive.int(tagOpts)
    for (let i = 0; i < size; i++) {
      // Append html for this tag
      if (key === "each") {
        for (let eachTag of tagOpts.tag) {
          blurb.push(html(wordList, eachTag))
        }
      } else {
        // Randomly select a tag from the list.
        let tag = faker.random.objectElement(tagOpts.tags)
        if (nestedTags.includes(tag)) {
          blurb.push(
            `\n<${tag}>\n` + html(wordList, tagOpts.nest) + `\n</${tag}>\n`
          )
        } else if (nanoTags.includes(tag)) {
          blurb.push(htmlNano(wordList, tag))
        } else if (shortTags.includes(tag)) {
          blurb.push(htmlShort(wordList, tag))
        } else {
          blurb.push(htmlLong(wordList, tag))
        }
      }
      if (tagOpts.below) {
        blurb.push(html(wordList, tagOpts.below))
      }
    }
  }
  return blurb.join("\n")
}

let util = {
  wordly: wordly,
  paragraphly: paragraphly,
}

let core = {
  words: words,
  paragraph: paragraph,
  paragraphs: paragraphs,
  html: html,
}

module.exports = {
  ...core,
  util: util,
}
