/** @file Erects columns of realistic, random data */
"use strict"
const _ = require("lodash")
const faker = require("faker")

const IAmPrimitive = require("./iamprimitive")
const IGossip = require("./igossip")
const IJustHelp = require("./ijusthelp")

/** @file Erect a column from a raw json file of raw data. */
const raw = (size, fieldDef) => {
  let erect = IJustHelp.reRaw(require(`../raw/${fieldDef.file}.json`))

  // Randomly shuffle before filtering.
  erect = faker.helpers.shuffle(erect)

  // Filter the raw data?
  if (fieldDef.filter && typeof fieldDef.filter === "object") {
    erect = erect.filter(e =>
      _.isEqual(_.pick(e, Object.keys(fieldDef.filter)), fieldDef.filter)
    )
  }

  if (!erect.length) {
    throw Error("No records found. Did you filter correctly?")
  }

  // Useful to return all records for using raw records in our functions.
  if (size === "all") {
    size = erect.length
  }

  // Lengthen if required.
  erect = new Array(Math.ceil(size / erect.length))
    .fill(erect)
    .flat()
    .splice(0, size)

  // Select 1, or more fields
  if (fieldDef.field) {
    if (typeof fieldDef.field === "string") {
      erect = erect.map(e => e[fieldDef.field])
    } else if (Array.isArray(fieldDef.field) && fieldDef.field.length === 1) {
      erect = erect.map(e => e[fieldDef.field[0]])
    } else {
      erect = erect.map(e => _.pick(e, fieldDef.field))
    }
  }
  return erect
}

/** @file Erect a column of an exact item. */
const exact = (size, fieldDef) => {
  return new Array(size).fill(fieldDef.value)
}

/** @file Erect a column selecting randomly from your choices. */
const quick = (size, fieldDef) => {
  // Repeat the list as many times as needed.
  let erect = new Array(Math.ceil(size / fieldDef.list.length))
    .fill(fieldDef.list)
    .flat()
    .splice(0, size)
  // Randomly shuffle it.
  return faker.helpers.shuffle(erect)
}

/** @file Erect a column by rotating through a list. */
const toothpaste = (size, fieldDef) => {
  return new Array(Math.ceil(size / fieldDef.list.length))
    .fill(fieldDef.list)
    .flat()
    .splice(0, size)
}

/** @file Erect a column using faker with mustache.
* @example
{ mustache: "{{name.lastName}}, {{name.firstName}} {{name.suffix}}" }
* @param {integer} size of column.
* @param {object} fieldDef
 */
const marak = (size, fieldDef) => {
  return new Array(size).fill().map(_ => faker.fake(fieldDef.mustache))
}

let primitive = (size, fieldDef) => {
  if (!IAmPrimitive.hasOwnProperty(fieldDef.method)) {
    throw Error(`IAmPrimitive does not have the ${fieldDef.method}`)
  }
  return new Array(size)
    .fill()
    .map(_ => IAmPrimitive[fieldDef.method](fieldDef))
}

let gossip = (size, fieldDef) => {
  let wordList = raw("all", fieldDef)
  if (!IGossip.hasOwnProperty(fieldDef.method)) {
    throw Error(`IGossip does not have the ${fieldDef.method}`)
  }
  return new Array(size)
    .fill()
    .map(_ => IGossip[fieldDef.method](wordList, fieldDef))
}

let concat = fieldDef => {
  let concatonatedLies = Object.values(fieldDef).map(l => l.lies)
  let zippedLies = _.zip(...concatonatedLies)
  return zippedLies.map(zL => zL.join(""))
}

let choose = fieldDef => {
  let concatonatedLies = Object.values(fieldDef).map(l => l.lies)
  let zippedLies = _.zip(...concatonatedLies)
  return zippedLies.map(zL => faker.random.objectElement(zL))
}

module.exports = {
  pk: (size, fieldDef) => _.times(size, n => n + 1),
  hash: (size, fieldDef) =>
    _.times(size, n => IJustHelp.cyrb53(n, fieldDef.seed)),
  raw: raw,
  exact: exact,
  quick: quick,
  toothpaste: toothpaste,
  marak: marak,
  primitive: primitive,
  gossip: gossip,
  concat: concat,
  choose: choose,
}
