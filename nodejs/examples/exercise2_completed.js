"use strict"
const ITransform = require("../liar/itransform")
const Pinocchio = require("../pinocchio")
const { rePurpose } = require("../liar/ijusthelp")

const { atSymbol } = require("../model/common")
const { personalDomainField } = require("../model/personal")

let numberOfRows = 20
let pinocchio = new Pinocchio(numberOfRows)

let pk = {
  name: "pk",
  class: "pk",
}

// A person defintion filtered to males.
let husband = {
  class: "raw",
  file: "person",
  flatten: true,
  filters: {
    sex: ["male"],
  },
}

// A person defintion filtered to married female titles.
let wife = {
  class: "raw",
  file: "person",
  flatten: true,
  filters: {
    sex: ["female"],
    title: ["Mrs", "Dr"],
  },
}

let lastName = {
  class: "raw",
  file: "lastname",
}

// A user name definition using the wifes's first name field.
let wifeUserName = {
  class: "field",
  field: "husband_firstName",
  transform: [ITransform.lower],
  remove: true,
}

// Copy the `wifeUserName` defintion and change it for the husband.
let husbandUserName = rePurpose(wifeUserName, {
  field: "wife_firstName",
})

// Define an email address for the husband and wife, using the `concat` class to build compound file.
let husbandEmail = {
  class: "concat",
  from: {
    husbandUserName,
    sep: {
      class: "quick",
      list: ["_", ".", "-", ""],
    },
    last: {
      class: "field",
      field: "lastName",
    },
    atSymbol,
    personalDomainField,
  },
  transform: [ITransform.lower],
}
let wifeEmail = {
  class: "concat",
  from: {
    wifeUserName,
    sep: {
      class: "quick",
      list: ["_", ".", "-", ""],
    },
    last: {
      class: "field",
      field: "lastName",
    },
    atSymbol,
    personalDomainField,
  },
  transform: [val => val.toLowerCase()],
}

let marriedCouple = {
  pk: pk,
  husband: husband,
  wife: wife,
  lastName: lastName,
  husbandUserName: husbandUserName,
  wifeUserName: wifeUserName,
  husbandEmail: husbandEmail,
  wifeEmail: wifeEmail,
}
let lies = pinocchio.lies(marriedCouple)
console.log(lies)
