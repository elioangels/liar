"use strict"
const ITransform = require("../liar/itransform")
const Pinocchio = require("../pinocchio")

let numberOfRows = 20
let pinocchio = new Pinocchio(numberOfRows)

let pk = {
  class: "hash",
}

let companyName = {
  class: "concat",
  from: {
    danishWord: {
      class: "gossip",
      method: "words",
      min: 1,
      max: 2,
      file: "danish",
      transform: [ITransform.title],
    },
    companyType: {
      class: "quick",
      list: [
        " Ltd",
        " Ltd",
        " Ltd",
        " Ltd",
        " Pty",
        " Corporation",
        " Plc",
        " Limited",
        " & Sons",
      ],
    },
  },
}

let companySize = {
  class: "primitive",
  method: "int",
  min: 5,
  max: 500,
}

let companyWorth = {
  class: "concat",
  from: {
    dollar: {
      class: "exact",
      value: "$ ",
    },
    amount: {
      class: "field",
      field: "companySize",
      transform: [val => val * 10000, Math.round],
    },
  },
}

let companyProspect = {
  pk: pk,
  companyName: companyName,
  companySize: companySize,
  companyWorth: companyWorth,
}

let lies = pinocchio.lie(companyProspect)
console.log(lies)
