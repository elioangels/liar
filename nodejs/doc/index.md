# pinocchio

<figure>
  <img src="star.png" alt="">
</figure>

> Fake it until you make it, **the elioWay**

![experimental](https://elioway.gitlab.io/artwork/icon/experimental/favicon.png "experimental")

A source of random but realistic looking data. This app works by combining randomly generated primitive values and prepared data to create realistic random records that you can use to test and demo your databases and apps.

<div><a href="installing.html">
  <button>Installing</button>
</a>
  <a href="quickstart.html">
  <button>Quickstart</button>
</a></div>

## Cult of Lies

**pinocchio** is a wrapper around Marak's **faker** module, which allows you to create models for faked data formatted in JSON to closly match the way you describe typical MVC models.

With a little massaging, you could even add a new property to your MVC models defining the fake data you'd like for it; then pass the MVC model directly into **pinocchio**.

It works in a similar way to **pinocchio**'s sister project [liar](https://elioway.gitlab.io/elioangels/liar/), written in **Python**, but uses **Javascript** to create fake data you define a model. An example would look like this:

```
let gamePlay = {
  dice1: { class: "primitive", method: "int", min: 1, max: 6 },
  dice2: { class: "primitive", method: "int", min: 1, max: 6 },
  player: { name: "player", class: "toothpaste", list: ["white", "black"] },
}
```

Or:

```
let localDoctors = {
  firstName: {
    class: "raw",
    file: "person",
    filters: { title: ["Dr"] },
    field: ["name"]
  },
  lastName: {
    class: "raw",
    file: "lastname"
  },
  email: {
    class: "concat",
    from: {
      first: { class: "field", field: "firstName" }
      last: { class: "field", field: "lastName" }
      at: { class: "exact", value: "@" },
      randomTLD: { class: "marak", mustache: ".{{internet.domainSuffix}}" },
    },
    transform: [ val => String.toLowerCase(val), ]
  }
}
```

Notice how the `email` field in this fake dataset will be concatonating the data from the other field. This works because **pinocchio** builds data for fields one by one. In this way, one field can reference and reuse the data from another field. _(This feature is discussed below.)_

You use like this:

```
const Pinocchio = require("./pinocchio")
let gamePlay = {
  dice1: { class: "primitive", method: "int", min: 1, max: 6 },
  dice2: { class: "primitive", method: "int", min: 1, max: 6 },
  player: { name: "player", class: "toothpaste", list: ["white", "black"] },
}
let numberOfRows = 6
let pinocchio = new Pinocchio(numberOfRows)
let lies = pinocchio.lie(gamePlay)
console.log(lies)
>> Outputs:
[
  { player: 'white', dice1: 1, dice2: 3 },
  { player: 'black', dice1: 2, dice2: 5 },
  { player: 'white', dice1: 2, dice2: 2 },
  { player: 'black', dice1: 3, dice2: 3 },
  { player: 'white', dice1: 3, dice2: 1 },
  { player: 'black', dice1: 6, dice2: 4 },
]
```

## Why do I need pinocchio when we already have faker?

Two main reasons.

### Reason One: Modelling

**pinocchio makes it easy to create and reuse definitions modelling fake data**

**faker**'s strength is providing single functions for calling lists of realistic looking, fake data. These are brilliant and work well. Because you can provide the seed, the data is consistent between sessions.

**pinocchio** respects this feature and it is a law here:

- same definition + same seed == same data

What **pinocchio** adds is the ability to define the data as an MVC type `Object`, then have the `Pinocchio` class manage all the faker (and other) calls for you, returning a list of dictionaries with the data ready to use.

If you have used **EmberJS** and the **Mirage** addon, you will be used to this. **Mirage** allows you to incorporate **faker** into its MVC framework of models. In much the same way, **pinocchio** facilitates incorporating fake data sets into a framework.

### Reason Two: Consistent

**pinocchio will ensure a record is consistent across all its fields.**

**faker**'s weakness can be illustrated by looking at the output of its own limited "model" feature calling the `helpers.userCard`

```
"name": "Bert Kertzmann",
"username": "Danyka.McDermott33",
"email": "Dominic71@yahoo.com",
```

The problem is that, for this 1 contact, the name is different across the fields. It looks like a different person, even though it's the same record. As a webdev I use fake data during development. That's why I want data to be recognisable; consistent; and clear. I want to know if my templates are working. Having data for each record which relates to the person or object helps visually assess the state of your application. That's what **pinocchio** allows you to do.

### More reasons

**pinocchio**:

- Comes with ready made definitions, so you can bolt together new defintions easily. Find them in the `model` folder.
- Comes with it's own, realistic raw data. Some of this duplicates what **faker** does; but there are additional data sets.
- Has a "transform" option you can use to run a series of transformation functions on the data post-creation. You can use any Javascript function which takes a value and transforms it, e.g. toLowerCase(); .trim(), **lodash** functions, etc.
- Has a "sprinkle" transform function so that you can sprinkle keywords into text; for instance sprinkling the product code into loremipsum text to make it more realistic.
- More...!

### elioWay Purpose

We added **pinocchio** to the **elioWay** to we use as an engine to generate fake data for any of the <https://Schema.org> models of `Things` in our [thing-liar Documentation](https://elioway.gitlab.io/eliothing/thing-liar/) module. Check it out because we defined models for hundreds of `Things` you can either reuse to create datasets for your own models; or, if you want to do things the **elioWay**, just use Schema! It's what we're all about.
