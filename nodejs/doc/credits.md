# pinocchio Credits

## Artwork

- <https://commons.wikimedia.org/wiki/File:Le_avventure_di_Pinocchio-pag184.jpg>
- <https://commons.wikimedia.org/wiki/File:Industria_veneziana_di_mobili_laccati,_cameretta_pinocchio,_1928_circa,_letto_01.jpg>

## Useful

- [bryc's cyrb53](https://stackoverflow.com/a/52171480/4464683)
- <https://github.com/dumbmatter/facesjs>
- <https://www.blobmaker.app/>
