# Installing pinocchio

## Prerequisites

- [elioWay Prerequisites](https://elioway.gitlab.io/installing.html)
- [elioangels Prerequisites](https://elioway.gitlab.io/elioangels/installing.html)

## npm

Install into your SASS projects.

```
npm install @elioway/pinocchio
yarn add @elioway/pinocchio
```

## Development

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/pinocchio.git
```
