const faker = require("faker")
const should = require("chai").should()

const IErect = require("../liar/ierect")

let TESTWORDS = "abcdefghijklmnopqrstuvwxyz".split("")

describe("liar | IErect | choose", () => {
  it("combines fields of lies into a single field of lies.", () => {
    faker.seed(12)
    IErect.choose({
      a: { lies: [1, 2, 3] },
      b: { lies: ["a", "b", "c"] },
      c: { lies: ["I", "V", "X"] },
    }).should.deep.eql([1, "b", "X"])
    faker.seed(75)
    IErect.choose({
      a: { lies: [1, 2, 3] },
      b: { lies: ["a", "b", "c"] },
      c: { lies: ["I", "V", "X"] },
    }).should.deep.eql(["a", "V", 3])
    faker.seed(28)
    IErect.choose({
      a: { lies: [1, 2, 3] },
      b: { lies: ["a", "b", "c"] },
      c: { lies: ["I", "V", "X"] },
    }).should.deep.eql(["I", 2, "c"])
    // NB: Results above contrived using "seed" to produce the above result. It
    // could just as easily be: [1, 2, 3] or ["a", "b", "c"] or ["I", "V", "X"]
    faker.seed(11)
    IErect.choose({
      a: { lies: [1, 2, 3] },
      b: { lies: ["a", "b", "c"] },
      c: { lies: ["I", "V", "X"] },
    }).should.deep.eql([1, 2, 3])
  })
})

describe("liar | IErect | concat", () => {
  it("combines fields of lies into a single field of lies.", () => {
    IErect.concat({
      a: { lies: [1, 2, 3] },
      b: { lies: ["a", "b", "c"] },
      c: { lies: ["I", "V", "X"] },
    }).should.deep.eql(["1aI", "2bV", "3cX"])
  })
})

describe("liar | IErect | exact", () => {
  it("creates a column of lies of a exact nature.", () => {
    IErect.exact(3, { value: 123 }).should.be.an("array")
    IErect.exact(3, { value: 123 }).should.have.lengthOf(3)
    IErect.exact(3, { value: 123 }).should.eql([123, 123, 123])
  })
})

describe("liar | IErect | gossip", () => {
  it("creates a column of lies of a gossipy nature.", () => {
    faker.seed(123)
    IErect.gossip(3, {
      method: "html",
      file: "ainu",
      p: { max: 1, min: 1 },
    }).should.be.an("array")
    IErect.gossip(3, {
      method: "paragraphs",
      file: "ainu",
      max: 3,
      min: 1,
    }).should.have.lengthOf(3)
    IErect.gossip(3, {
      method: "words",
      file: "ainu",
      max: 3,
      min: 1,
    }).should.eql(["sineani casi", "ekaci ray hosipika", "sosiekatta tani"])
  })
})

describe("liar | IErect | hash", () => {
  it("creates a column of lies of a hashy nature.", () => {
    IErect.hash(3, {}).should.be.an("array")
    IErect.hash(3, {}).should.have.lengthOf(3)
    IErect.hash(3, { seed: 3 }).should.eql([
      "1318413225951232f40567e3",
      "349793732997939250db4c54",
      "1485268410433536e4fa6648",
    ])
  })
})

describe("liar | IErect | marak", () => {
  it("creates a column of lies of a Marak nature.", () => {
    faker.seed(123)
    IErect.marak(3, { mustache: "{{vehicle.model}}" }).should.be.an("array")
    IErect.marak(3, { mustache: "{{internet.ip}}" }).should.have.lengthOf(3)
    IErect.marak(3, {
      mustache: "{{finance.currencySymbol}} {{finance.amount}}",
    }).should.eql(["MT 480.93", "BZ$ 392.12", "¥ 343.18"])
  })
})

describe("liar | IErect | pk", () => {
  it("creates a column of lies of a primary nature.", () => {
    IErect.pk(3, {}).should.be.an("array")
    IErect.pk(3, {}).should.have.lengthOf(3)
    IErect.pk(3, {}).should.eql([1, 2, 3])
  })
})

describe("liar | IErect | primitive", () => {
  it("creates a column of lies of a primitive nature.", () => {
    faker.seed(123)
    IErect.primitive(3, { method: "float", max: 5, min: 1 }).should.be.an(
      "array"
    )
    IErect.primitive(3, {
      method: "bool",
      max: 15,
      min: 10,
    }).should.have.lengthOf(3)
    IErect.primitive(3, { method: "int", max: 5, min: 1 }).should.eql([3, 4, 4])
  })
})

describe("liar | IErect | quick", () => {
  it("creates a column of lies of a quickening nature.", () => {
    faker.seed(123)
    IErect.quick(3, { list: [1, 2, 3] }).should.be.an("array")
    IErect.quick(3, { list: [1, 2, 3] }).should.have.lengthOf(3)
    IErect.quick(3, { list: [1, 2, 3] }).should.eql([3, 2, 1])
    IErect.quick(3, { list: [1, 2, 3] }).should.eql([1, 3, 2])
  })
})

describe("liar | IErect | raw", () => {
  it("creates a column of raw lies.", () => {
    faker.seed(123)
    IErect.raw(3, { file: "business_type" }).should.be.an("array")
    IErect.raw(3, { file: "business_type" }).should.have.lengthOf(3)
    IErect.raw(3, { file: "business_type" }).should.eql([
      "Inc",
      "Corporation",
      "& Son",
    ])
  })
  it("gets the same data with the same seed.", () => {
    faker.seed(123)
    IErect.raw(3, { file: "business_type" }).should.be.an("array")
    IErect.raw(3, { file: "business_type" }).should.have.lengthOf(3)
    IErect.raw(3, { file: "business_type" }).should.eql([
      "Inc",
      "Corporation",
      "& Son",
    ])
  })
  it("gets different data every time in a session with the same seed.", () => {
    faker.seed(123)
    IErect.raw(3, { file: "business_type" }).should.be.an("array")
    IErect.raw(3, { file: "business_type" }).should.have.lengthOf(3)
    IErect.raw(3, { file: "business_type" }).should.eql([
      "Inc",
      "Corporation",
      "& Son",
    ])
    IErect.raw(3, { file: "business_type" }).should.eql([
      "Associated",
      "& Son",
      "Incorporated",
    ])
    IErect.raw(3, { file: "business_type" }).should.eql([
      "International",
      "Ltd",
      "Incorporated",
    ])
  })
  it("filters the column before spitting out the lies.", () => {
    faker.seed(123)
    IErect.raw(2, {
      file: "color",
      filter: { color: "IndianRed" },
    }).should.eql([
      {
        color: "IndianRed",
        hex: "#CD5C5C",
        shade: "red",
        common: false,
        r: 205,
        g: 92,
        b: 92,
      },
      {
        color: "IndianRed",
        hex: "#CD5C5C",
        shade: "red",
        common: false,
        r: 205,
        g: 92,
        b: 92,
      },
    ])
    IErect.raw(20, {
      file: "color",
      filter: { r: 240, common: false },
    }).should.deep.include({
      color: "LightCoral",
      hex: "#F08080",
      shade: "red",
      common: false,
      r: 240,
      g: 128,
      b: 128,
    })
    IErect.raw(20, {
      file: "color",
      filter: { r: 240, common: false },
    }).should.not.deep.include({
      color: "Khaki",
      hex: "#F0E68C",
      shade: "yellow",
      common: true,
      r: 240,
      g: 230,
      b: 140,
    })
  })
  it("returns a slimmer column of lies.", () => {
    faker.seed(123)
    IErect.raw(3, { file: "color", field: ["color", "hex"] }).should.eql([
      { color: "Beige", hex: "#F5F5DC" },
      { color: "LightBlue", hex: "#ADD8E6" },
      { color: "LightGreen", hex: "#90EE90" },
    ])
    IErect.raw(3, { file: "color", field: ["color"] }).should.eql([
      "MediumSlateBlue",
      "DarkCyan",
      "Sienna",
    ])
    IErect.raw(3, { file: "color", field: "color" }).should.eql([
      "MediumBlue",
      "SaddleBrown",
      "FloralWhite",
    ])
  })
})

describe("liar | IErect | toothpaste", () => {
  it("creates a column of lies of a stripy nature.", () => {
    faker.seed(123)
    IErect.toothpaste(3, { list: [1, 2, 3] }).should.be.an("array")
    IErect.toothpaste(3, { list: [1, 2, 3] }).should.have.lengthOf(3)
    IErect.toothpaste(9, { list: [1, 2, 3] }).should.eql([
      1,
      2,
      3,
      1,
      2,
      3,
      1,
      2,
      3,
    ])
  })
})
