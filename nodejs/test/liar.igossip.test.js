const faker = require("faker")
const should = require("chai").should()

const IGossip = require("../liar/igossip")

let TESTWORDS = "abcdefghijklmnopqrstuvwxyz".repeat(2).split("")

describe("liar | IGossip | words | ", () => {
  it("creates words lies", () => {
    faker.seed(123)
    IGossip.words([...TESTWORDS], { max: 6, min: 3 }).should.be.a("string")
    IGossip.words([...TESTWORDS], { max: 6, min: 3 }).should.eql("y b n f")
  })
})
describe("liar | IGossip | paragraph | ", () => {
  it("creates paragraph lies", () => {
    faker.seed(123)
    IGossip.paragraph([...TESTWORDS], { max: 6, min: 3 }).should.be.a("string")
    IGossip.paragraph([...TESTWORDS], { max: 6, min: 3 }).should.eql("Y b n f.")
  })
})
describe("liar | IGossip | paragraphs | ", () => {
  it("creates paragraphs lies", () => {
    faker.seed(123)
    IGossip.paragraphs([...TESTWORDS], { max: 3, min: 2 }).should.be.a("string")
    IGossip.paragraphs([...TESTWORDS], { max: 3, min: 2 }).should.include(
      `Y u j d e p r g s x o b v c x u o z t o! V c a h t l y k i h f h n j k o o m f a y d d v l z q q q m l z i g, g s t q u i w g. T p f j x, y x q r i q a x u s? E y p, e k o j n n a e g z e a b a c u c, w i d n k b r k i c s t h u y l l k e e p o.

I n p r h o j u g m q u o f a a f s, n w j z p y d c k c b z, r j. O e. G t y z h l q i l s, r d f t d v x e l c o c s m v.`
    )
  })
})
describe("liar | IGossip | html", () => {
  it("creates html lies", () => {
    faker.seed(123)
    let html = IGossip.html([...TESTWORDS], {
      p: { tags: ["p"], max: 1, min: 1 },
    })
    html.should.include(
      `<p>Z c i z n v t g o j y g q v q r s x, k w w j a z g l d r w i h z b o f d n p s p o, v d e j s y p v r l a b c t r d l y m g m h n u f u f x e u k b k c e l j.</p>`
    )
  })
  it("conforms to elioSin", () => {
    faker.seed(123)
    let html = IGossip.html([...TESTWORDS], {
      h3: {
        tags: ["h3"],
        max: 1,
        min: 1,
        below: {
          blockquote: {
            tags: ["blockquote", "aside"],
            max: 1,
            min: 1,
          },
          p: {
            tags: ["p", "section", "article"],
            max: 1,
            min: 1,
          },
        },
      },
    })
    html.should.include(
      `<h3>W d d g e p n q.</h3>
<blockquote>U e g k o b i p i t o x r, k y h c q j m.</blockquote>
<article>A o p m v r o x w q g p l m z j j c x p r f u h c d c y s f z p b a k u z j n m o g e f y. M t e d q t l e c v d v s m. B r i u t z k y j h a x t g w o j d u y a g a t u f q, o l t s s n y c z s l k o o q z, x e f o r v i n u j e q n u w h c g m v v v d i i n r b a v t e h k.</article>`
    )
  })
  it("shoots html bullet of lies", () => {
    faker.seed(123)
    let html = IGossip.html([...TESTWORDS], {
      ul: {
        tags: ["ol", "ul"],
        max: 1,
        min: 1,
        nest: {
          li: {
            tags: ["li"],
            max: 6,
            min: 2,
          },
        },
      },
    })
    html.should.include(
      `<ul>
<li>I o l a x n n.</li>
<li>J t p i y l z h d r k n d s.</li>
<li>T f h d x o c g g u.</li>
</ul>`
    )
  })
  it("creates html tables of lies", () => {
    faker.seed(123)
    let html = IGossip.html([...TESTWORDS], {
      table: {
        tags: ["table"],
        max: 1,
        min: 1,
        nest: {
          thead: {
            tags: ["thead"],
            max: 1,
            min: 1,
            nest: {
              tr: {
                tags: ["tr"],
                max: 1,
                min: 1,
                nest: {
                  th: {
                    tags: ["th"],
                    max: 3,
                    min: 3,
                  },
                },
              },
            },
          },
          tbody: {
            tags: ["tbody"],
            max: 1,
            min: 1,
            nest: {
              tr: {
                tags: ["tr"],
                max: 3,
                min: 2,
                nest: {
                  td: {
                    tags: ["td"],
                    max: 3,
                    min: 3,
                  },
                },
              },
            },
          },
        },
      },
    })
    html.should.include(
      `<table>

<thead>

<tr>
<th>H j e.</th>
<th>N v.</th>
<th>Z l p.</th>
</tr>

</thead>


<tbody>

<tr>
<td>L.</td>
<td>Q n.</td>
<td>J d w.</td>
</tr>


<tr>
<td>W t.</td>
<td>T n e.</td>
<td>X p w.</td>
</tr>


<tr>
<td>S o.</td>
<td>E q.</td>
<td>O v.</td>
</tr>

</tbody>

</table>`
    )
  })

  it("creates html datalist of lies", () => {
    faker.seed(123)
    let html = IGossip.html([...TESTWORDS], {
      dl: {
        tags: ["dl"],
        max: 1,
        min: 1,
        nest: {
          each: {
            tag: [
              {
                dt: {
                  tags: ["dt"],
                  max: 1,
                  min: 1,
                },
              },
              {
                dd: {
                  tags: ["dd"],
                  max: 1,
                  min: 1,
                },
              },
            ],
            max: 2,
            min: 2,
          },
        },
      },
    })
    html.should.include(
      `<dl>
<dt>V r m.</dt>
<dd>F a y m w p v y q p u d e t h k c g z z g l o o j s l x q k s n b j e r m n d t h x i v b.</dd>
<dt>L o u.</dt>
<dd>P a c n v p q u z w i l y w b f v f q r t a e? H d k x b s z.</dd>
</dl>`
    )
  })
})
describe("liar | IGossip | util | wordly", () => {
  it("creates lists of word lies", () => {
    faker.seed(123)
    IGossip.util
      .wordly([...TESTWORDS], { max: 6, min: 3 })
      .should.be.an("array")
    IGossip.util
      .wordly([...TESTWORDS], { max: 6, min: 3 })
      .length.should.be.gte(3)
    IGossip.util
      .wordly([...TESTWORDS], { max: 6, min: 3 })
      .length.should.be.lte(6)
    IGossip.util
      .wordly([...TESTWORDS], { max: 6, min: 3 })
      .should.eql(["e", "i", "h"])
  })
  it("increases word size", () => {
    faker.seed(123)
    IGossip.util
      .wordly([...TESTWORDS], { max: 600, min: 300 })
      .length.should.be.gte(300)
    IGossip.util
      .wordly([...TESTWORDS], { max: 600, min: 300 })
      .length.should.be.lte(600)
  })
})
describe("liar | IGossip | util | paragraphly", () => {
  it("creates lists of paragraph lies", () => {
    faker.seed(123)
    IGossip.util
      .paragraphly([...TESTWORDS], { max: 6, min: 3 })
      .should.be.an("array")
    IGossip.util
      .paragraphly([...TESTWORDS], { max: 6, min: 3 })
      .length.should.be.gte(3)
    IGossip.util
      .paragraphly([...TESTWORDS], { max: 6, min: 3 })
      .length.should.be.lte(6)
    IGossip.util
      .paragraphly([...TESTWORDS], { max: 6, min: 3 })
      .should.eql(["E", "i", "h", "."])
  })
})
