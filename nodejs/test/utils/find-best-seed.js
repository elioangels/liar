const faker = require("faker")
const Pinocchio = require("../../pinocchio")

// require("fs").writeFileSync(`./expected.json`, JSON.stringify(lies))

module.exports = (lieDef, twoFieldsEqual) => {
  for (let seed = 1; seed < 999; seed++) {
    faker.seed(seed)
    let pinocchio = new Pinocchio(2)
    let lies = pinocchio.lie(lieDef)
    let foundEqual = twoFieldsEqual.filter(field => {
      console.log(
        seed,
        field,
        lies[0][field],
        lies[1][field],
        lies[0][field] === lies[1][field]
      )
      return lies[0][field] === lies[1][field]
    })
    console.log(seed, foundEqual)
    if (!foundEqual.length) {
      return seed
    }
  }
}
