const faker = require("faker")
const _ = require("lodash")
const should = require("chai").should()

const ITransform = require("../liar/itransform")

describe("liar | ITransform", () => {
  it("capitalizes", () => {
    ITransform.capitalize("i am a wizard").should.eql("I am a wizard")
  })
  it("chomps", () => {
    ITransform.chomp("i am a wizard").should.eql("i")
  })
  it("lowers", () => {
    ITransform.lower("I Am A WIZARD").should.eql("i am a wizard")
  })
  it("slugifies", () => {
    ITransform.slugify("i am a wizard").should.eql("i-am-a-wizard")
  })
  it("despaces", () => {
    ITransform.spaceless("i am a wizard").should.eql("iamawizard")
  })
  it("despaces", () => {
    ITransform.spaceless("i am a wizard").should.eql("iamawizard")
  })
  it("titles", () => {
    ITransform.title("i am a wizard").should.eql("I Am A Wizard")
  })
  it("trims", () => {
    ITransform.trim("   i am a wizard   ").should.eql("i am a wizard")
  })
  it("uppers", () => {
    ITransform.upper("i am a wizard").should.eql("I AM A WIZARD")
  })
})

describe("liar | ITransform | funky", () => {
  it("appends", () => {
    let appendNameOfThing = ITransform.funky.append("Wizard")
    appendNameOfThing("I Am A").should.eql("I Am A Wizard")
  })
  it("splutters", () => {
    faker.seed(123)
    let splutterer = ITransform.funky.splutter(70)
    splutterer("1").should.eql("")
    splutterer("2").should.eql("2")
    splutterer("3").should.eql("")
    splutterer("4").should.eql("")
    splutterer("5").should.eql("")
    splutterer("6").should.eql("")
    splutterer("7").should.eql("")
    splutterer("8").should.eql("8")
    splutterer("9").should.eql("9")
    splutterer("10").should.eql("")
    splutterer("11").should.eql("")
    splutterer("12").should.eql("12")
    splutterer("13").should.eql("13")
    splutterer("14").should.eql("")
    splutterer("15").should.eql("")
    splutterer("16").should.eql("")
    splutterer("17").should.eql("")
    splutterer("18").should.eql("")
    splutterer("19").should.eql("")
    splutterer("20").should.eql("")
  })
  it("sprinkles", () => {
    faker.seed(123)
    let val = `Y u j d e p r g s x o b v c x u o z t o! V c a h t l y k i h f h n j k o o m f a y d d v l z q q q m l z i g, g s t q u i w g. T p f j x, y x q r i q a x u s? E y p, e k o j n n a e g z e a b a c u c, w i d n k b r k i c s t h u y l l k e e p o.

  I n p r h o j u g m q u o f a a f s, n w j z p y d c k c b z, r j. O e. G t y z h l q i l s, r d f t d v x e l c o c s m v.`
    let sprinkle = ITransform.funky.sprinkle({
      punctuate: ["Wizard", "Apprentice"],
      densage: 30,
    })
    let sprinkled = sprinkle(val)
    sprinkled.should.eql(
      `Y u j d e p r g s x o b v c x u o z t o! V c a h t l y k i h f h n j k o o m f a y d d v l z q q q m l z i g, g s t q u i w g. T p f j x, y x q r i q a x u s? E y p, e k o j n n a e g z e a b a c Apprentice u c, w i d n k b r k i c s t h Wizard u y l l k e e p o.

  I n p r h o j u g Apprentice m q u o f a a f s, n w j z p y d c k c b z, r j. O e. G t y Wizard z h l q i Apprentice l s, Wizard r d f t d v x e l c o c s m v.`
    )
  })
})
