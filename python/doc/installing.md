# Installing liar

## Installing with pip

In an existing python project, pip install **liar**.

```
pip install elio-liar
```

If you want to create data for an existing non-python project, install **liar** using the instructions below and export your random data as JSON files your can use as required in your projects.

## Development

### Setup

.. is also a fine way to install **liar**, using GIT as a starting project.

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/liar.git
cd liar
virtualenv --python=python3 venv-liar
source venv-liar/bin/activate
pip install -r requirements/local.txt
```

**Run the tests:**

```shell
find . -name '*.pyc' -delete
find . -name '__pycache__' -delete
py.test -x
```

**Run the basic script:**

```shell
python examples/harry_potter.py
```

### Publish

Activate the virtualenv.

```
python3 setup.py sdist bdist_wheel
twine upload dist/*
# Enter YOUR-USERNAME and YOUR-PASSWORD
```

Testing it:

```
pip install elio-liar
```

**Test Publish**

```
python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
# Enter YOUR-USERNAME and YOUR-PASSWORD
cd some/test/folder
virtualenv --python=python3 venv-liar
source venv-liar/bin/activate
# or
source venv-liar/bin/activate.fish
python3 -m pip install --index-url https://test.pypi.org/simple/ liar
```
