# Using Ready-Made Data Definitions and Models

## Introduction

**liar** comes with heaps of ready made data definitions you can use. They can be found in the `liar/liar/model` folder and are listed below.

The names should be self explanatory. If in doubt, it's best to check the definition. It should be noted that many of the definitions are copies of base definitions where we simply overwrite the relevant properties.

## How Data Definitions are based on each other

For instance, the `in_24` data definition in `liar/liar/model/int.py` looks like this:

```python
in_24 = int_data("in_24", 1, 24)
```

`int_data` is a simple method which changes the `min` and `max` settings of a base `zero_or_one` data defintion:

```python
zero_or_one = rewrite_dict(int_list, {
    "name": "zero_or_one",
    "min": 0,
    "max": 1,
})


def int_data(name, min, max):
    return rewrite_dict(zero_or_one, {"name":name, "min":min, "max":max})
```

`rewrite_dict` is a commonly used utility method found in the `liar/liar/ijusthelp.py` file for deeply copying and rewriting data definitions.

```python
def rewrite_dict(source_dict, diffs):
    """Returns a copy of source_dict, updated with the new key-value
       pairs in diffs."""
    result = copy.deepcopy(source_dict)
    result.update(diffs)
    return result
```

`int_list` is a base data definition for creating randon integers from 0 to 1000:

```python
iamprimitive = {
    "name": "iamprimitive",
    "class": "iamprimitive",
    "method": "int_list",
    "min": 0,
    "max": 1,
}

int_list = rewrite_dict(iamprimitive, {
    "name": "int_list",
    "method": "int_list",
    "min": 0,
    "max": 1000,
})
```

So `in_24` is a data defintion for integers between 1 and 24, with a field name "in_24".

It's a little complicated, but you soon get used to it. Reusing data defintions helps avoid bugs!

## Using the Data Definitions

To use, simply import and list them in a model definition.

For example, a **contact list**:

```python
from liar.iamaliar import IAmALiar
number_records = 10
maker = IAmALiar(number_records)

from liar.model.marketing import company_name, job_title
from liar.model.personal import first_name, last_name, personal_email

model_def = [
  first_name,
  last_name,
  personal_email,
  personal_phone,
  company_name,
  job_title
]

data_set = maker.get_data(model_def)
```

Or **train departure times**:

```python
from liar.iamaliar import IAmALiar
number_records = 10
maker = IAmALiar(number_records)

from liar.ijusthelp import rename_def, rewrite_dict

from liar.model.location import geo_location
europe_destination = rewrite_dict(geo_location, {
    "name": "destination",
    "property": "city",
    "filters": {"iso2": ["GB", "FR", "DE", "IT",]},
})

from liar.model.time import within_hours
departure = rename_def(within_hours, "departure")

from liar.model.int import in_50
platform_number = rename_def(in_50, "platform_number")


model_def = [
  europe_destination,
  departure_time,
  platform_number
]

data_set = maker.get_data(model_def)
```

## Models

Models are ready made groupings of data definitions into what will surely be commonly used database model scenarios, like `product_model` or `standard_address_model` or `business_model` or `teen_model`. For instance `teen_model` has data defintions like first_name or last_name but with a date of birth scoped to teenagers. Therefore `teen_model` would be a perfect base model mocking a database for a youth club.

The ready made models are also listed below.

## liar.model.raw

See: [Raw](raw.html)

## liar.model.int

```python
from liar.model.int import int_model
```

**Available definitions**

- `zero_or_one`
- `one_or_two`
- `in_3`
- `in_4`
- `in_5`
- `dice`
- `in_7`
- `in_8`
- `in_9`
- `in_10`
- `in_11`
- `dozen`
- `in_20`
- `in_24`
- `in_50`
- `in_100`
- `in_1000`
- `in_10000`
- `town_populations`
- `city_populations`
- `country_populations`
- `stars_in_galaxy`

**Available models**

None

## liar.model.float

```python
from liar.model.float import float_model
```

**Available definitions**

- `quantum`
- `fractional`
- `grocery_price`
- `consumer_price`
- `gadget_price`
- `car_price`
- `house_price`
- `national_gdp`

**Available models**

None

## liar.model.time

```python
from liar.model.time import time_model
```

**Available definitions**

- `last_minute`
- `last_hour`
- `within_minutes`
- `within_hours`
- `opening_hours`
- `last_24`

**Available models**

None

## liar.model.date

```python
from liar.model.date import date_model
```

**Available definitions**

- `date_of_birth`
- `date_of_birth_oap`
- `date_of_birth_adult`
- `date_of_birth_parent`
- `date_of_birth_student`
- `date_of_birth_teen`
- `date_of_birth_child`
- `date_of_birth_toddler`
- `date_of_birth_baby`
- `department`
- `day_of_week`
- `day_of_week_abrev`
- `month_of_year`
- `month_of_year_abrev`
- `last_week`
- `last_month`
- `last_6_months`
- `last_year`
- `last_2_years`
- `last_decade`
- `last_century`
- `within_days`
- `within_weeks`
- `within_months`
- `within_12_months`
- `within_years`
- `sci_fi`

**Available models**

None

## liar.model.location

```python
from liar.model.location import location_model, standard_address_model
```

**Available definitions**

- `posh_address1`
- `address1`
- `address2`
- `locality`
- `geo_location`
- `post_code`

**Available models**

- `standard_address_model`
- `posh_address_model`

## liar.model.blurb

```python
from liar.model.blurb import blurb_model
```

**Available definitions**

- `blurb`
- `ainu_blurb`
- `danish_blurb`
- `english_blurb`
- `latin_blurb`
- `ainu_title`
- `danish_title`
- `english_title`
- `latin_title`
- `latin_motto`

**Available models**

None

## liar.model.business

```python
from liar.model.business import business_model
```

**Available definitions**

- `employee_number`
- `company_name`
- `company_address1`
- `company_address2`
- `company_locality`
- `company_geo_location`
- `company_post_code`
- `department`
- `job_title`
- `company_slug`
- `company_domain`
- `company_email`
- `info_email`
- `contact_email`
- `sales_email`
- `web_email`
- `company_web`
- `company_phone`
- `company_fax`

**Available models**

- `business_model`

## liar.model.marketing

```python
from liar.model.marketing import marketing_model
```

**Available definitions**

- `postage_cost`
- `product_weight`
- `news_title`
- `product_title`
- `product_name`
- `tweet`
- `description`
- `short_blurb`
- `blurb`
- `long_blurb`
- `essay`
- `foreign_chars`

**Available models**

- `marketing_model`
- `product_model`

## liar.model.personal

```python
from liar.model.personal import personal_model
```

**Available definitions**

- `title`
- `first_name`
- `last_name`
- `gender`
- `user_name`
- `home_address1`
- `home_address2`
- `home_locality`
- `home_geo_location`
- `home_post_code`
- `personal_domain`
- `personal_email`
- `personal_web`
- `personal_phone`
- `personal_mobile`
- `skin_tone`
- `hair_color`
- `accessory_color`

**Available models**

- `personal_model`
- `oap_model` = personal_model + [date_of_birth_oap]
- `adult_model` = personal_model + [date_of_birth_adult]
- `parent_model` = personal_model + [date_of_birth_parent]
- `student_model` = personal_model + [date_of_birth_student]
- `teen_model` = personal_model + [date_of_birth_teen]
- `child_model` = personal_model + [date_of_birth_child]
- `toddler_model` = personal_model + [date_of_birth_toddler]
- `baby_model` = personal_model + [date_of_birth_baby]
- `employee_model` = adult_model + [employee_number, company_name, department, job_title]

## liar.model.common

```python
from liar.model.common import common_model
```

**Available definitions**

- `pk`
- `space`
- `nb_space`
- `animal_name`
- `compound_word`

**Available models**

None

## Next

- [Using Ready-Made Raw Data](raw.html)
