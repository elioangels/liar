# Raw lies

## Introduction

**liar** comes with some pre-prepared data in a raw form. These can be used directly, or as the building blocks for creating new data; or as a boilerplate for add your own raw data.

The data can be access easily using the `IAmALiar` module by creating a data definition with `"class": "igetraw"` and naming the data source relating to the file containing the raw data in the `liar/liar/raw` folder, e.g. `"data": "surname"` You can add your own!

Below we describe how to use the raw data, and what properties you can expect. Although they are very easy to write, the data definitions for raw data have already been conveniently created for you in the `liar/model/raw.py` file. Each example shows how to write what is already written in this file.

All the examples below assume you have imported and instantiated the `IAmALiar` model as follows:

```
from liar.iamaliar import IAmALiar
number_records = 10
maker = IAmALiar(number_records)
```

All the examples below assume a usage where you access a simple data model and save the random data in a loop.

```
for row in data_set:
    data = Model()
    data.FIELD1 = row["raw_data"]["field1"]
    data.FIELD2 = row["raw_data"]["field2"]
    data.save()
```

_Your usage case may vary from this._

Here is a description of all the raw data which is available.

## `liar.raw.academic`

Contains a list of Academic study areas.

```
# Use:
from liar.model.raw import academic_raw

# Or from scratch:
academic_raw = {
    "name": "academic_raw",
    "class": "igetraw",
    "data": "academic",
}

data_set = maker.get_data([academic_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.subject = row["academic_raw"]["subject"]
    data.department = row["academic_raw"]["department"]
    data.save()
```

## `liar.raw.ainu`

Ainu is an old japanese language. The characterset seems familiar to speakers of romantic languages, while making little sense to the non-speaker.

```
# Use:
from liar.model.raw import ainu_raw

# Or from scratch:
ainu_raw = {
    "name": "ainu_words",
    "class": "igetraw",
    "data": "ainu",
}

data_set = maker.get_data([ainu_title])
```

Result

```
for row in data_set:
    data = Model()
    data.ainu_title = row["ainu_title"]
    data.save()
```

Using Ainu words on their own is great for product names, but they also make a useful alternative choice instead of the usual Latin blurb.

```
ainu_title = {
    "name": "ainu_title",
    "class": "iblurb",
    "language": "ainu",
    "method": "plaintext_title",
    "min": 3,
    "max": 5,
}

data_set = maker.get_data([ainu_title])
```

Result

```
for row in data_set:
    data = Model()
    data.title = row["ainu_title"]
    data.save()
```

## `liar.raw.animal_prefix`

A list of words commonly used to prefix animal names. This class is used in the living thing class to create random, but realistic, common animal names.

```
# Use:
from liar.model.raw import animal_prefix_raw

# Or from scratch:
animal_prefix_raw = {
    "name": "animal_prefix_raw",
    "class": "igetraw",
    "data": "animal_prefix",
}

data_set = maker.get_data([animal_prefix_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.animal_prefix = row["animal_prefix_raw"]
    data.save()
```

## `liar.raw.animal_suffix`

A list of words commonly used to suffix animal names. This class is used in the living thing class to create random, but realistic, common animal names.

```
# Use:
from liar.model.raw import animal_suffix_raw

# Or from scratch:
animal_suffix_raw = {
    "name": "animal_suffix_raw",
    "class": "igetraw",
    "data": "animal_suffix",
}

data_set = maker.get_data([animal_suffix_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.animal_suffix = row["animal_suffix_raw"]
    data.save()
```

## `liar.raw.animal`

The data is a compiled list of well known animal, insect and plant names, each classified (Mammal, Reptile, Plant, Bug, etc) and a 200x240px or 240x200px image from the public domain.

```
# Use:
from liar.model.raw import animal_raw

# Or from scratch:
animal_raw = {
    "name": "animal_raw",
    "class": "igetraw",
    "data": "animal",
}

data_set = maker.get_data([animal_suffix_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.animal = row["animal_raw"]["animal"]
    data.classification = row["animal_raw"]["classification"]
    data.drawing = row["animal_raw"]["images"]["drawing"]
    data.save()
```

Animal images are stored in "liar/liar/rawimages/animals/{img}.gif"

If you only need the animal name, use the property option:

```
# Use:
from liar.model.raw import animal_name_raw

# Or from scratch:
animal_name_raw = {
    "name": "animal_name_raw",
    "class": "igetraw",
    "data": "animal",
    "property": "animal"
}

data_set = maker.get_data([animal_name_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.animal_name = row["animal_name_raw"]
    data.save()
```

## `liar.raw.business_type`

Business Name suffixes.

```
# Use:
from liar.model.raw import business_type_raw

# Or from scratch:
business_type_raw = {
    "name": "business_type_raw",
    "class": "igetraw",
    "data": "business_type",
}

data_set = maker.get_data([business_type_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.business_type = row["business_type_raw"]
    data.save()
```

## `liar.raw.car`

This large list of real cars (uk models) might be useful to teachers for projects and classes, or perhaps scientists to test data modelling software. Each car in the collection has a model name, a make class - which includes the manufacturer's name and a badge - and are grouped into types (e.g. hatchback, saloon).

```
# Use:
from liar.model.raw import car_raw

# Or from scratch:
car_raw = {
    "name": "car_raw",
    "class": "igetraw",
    "data": "car",
}

data_set = maker.get_data([car_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.make = row["car_raw"]["make"]
    data.model = row["car_raw"]["model"]
    data.shape = row["car_raw"]["shape"]
    data.save()
```

## `liar.raw.char`

A single character.

```
# Use:
from liar.model.raw import char_raw

# Or from scratch:
char_raw = {
    "name": "char_raw",
    "class": "igetraw",
    "data": "char",
}

data_set = maker.get_data([char_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.ascii = row["char_raw"]["ascii"]
    data.lower = row["char_raw"]["lower"]
    data.upper = row["char_raw"]["upper"]
    data.digits = row["char_raw"]["digit"]
    data.hexdigits = row["char_raw"]["hexdigit"]
    data.octdigits = row["char_raw"]["octdigit"]
    data.printable = row["char_raw"]["printable"]
    data.punctuation = row["char_raw"]["punctuation"]
    data.whitespace = row["char_raw"]["whitespace"]
    data.save()
```

The char data is compiled from these raw data sets:

```
ascii_letter_raw = {"name": "char_raw", "data": "ascii_letter"}
ascii_lowercase_raw = {"name": "ascii_lowercase_raw", "data": "ascii_lowercase"}
ascii_uppercase_raw = {"name": "ascii_uppercase_raw", "data": "ascii_uppercase"}
digit_raw = {"name": "digit_raw", "data": "digit"}
hexdigit_raw = {"name": "hexdigit_raw", "data": "hexdigit"}
octdigit_raw = {"name": "octdigit_raw", "data": "octdigit"}
printable_raw = {"name": "printable_raw", "data": "printable"}
punctuation_raw = {"name": "punctuation_raw", "data": "punctuation"}
whitespace_raw = {"name": "whitespace_raw", "data": "whitespace"}
```

## `liar.raw.colors`

A list of colors supported on the web.

```
# Use:
from liar.model.raw import colors_raw

# Or from scratch:
colors_raw = {
    'name': 'colors_raw',
    'class': 'igetraw',
    'data': 'colors',
    'filter': {'common': [True]}
}

data_set = maker.get_data([colors_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.color = row['colors_raw']["color"]
    data.save()
```

## `liar.raw.danish`

Simply a list of Danish words, without meanings. Because of the common use of accented characters, this makes the data useful to developers looking to rigidly test their applications. The data can also be used to create the names of far-out products, or even fictional character names.

```
# Use:
from liar.model.raw import danish_raw

# Or from scratch:
danish_raw = {
    "name": "danish_raw",
    "class": "igetraw",
    "data": "danish",
}

data_set = maker.get_data([danish_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.danish_word = row["danish_raw"]
    data.save()
```

Blurb:

```
danish_title = {
    "name": "danish_title",
    "class": "iblurb",
    "language": "danish",
    "method": "plaintext_title",
    "min": 5,
    "max": 8,
}

data_set = maker.get_data([danish_title])
```

Result

```
for row in data_set:
    data = Model()
    data.danish_title = row["danish_title"]
    data.save()
```

## `liar.raw.district_suffix`

A list of words commonly used to suffix urban and country locations.

```
# Use:
from liar.model.raw import district_suffix_raw

# Or from scratch:
district_suffix_raw = {
    "name": "district_suffix_raw",
    "class": "igetraw",
    "data": "district_suffix",
}

data_set = maker.get_data([district_suffix_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.district_suffix = row["district_suffix_raw"]
    data.save()
```

Create a definition which concatonates a latin word to a district_suffix

```
# Use:
from liar.model.raw import latin_raw

# Or from scratch:
latin_raw = {
    "name": "latin_raw",
    "class": "igetraw",
    "data": "latin",
}

locality = {
    "name": "locality_field",
    "class": "concat",
    "itransform": ["capitalize"],
    "data": [latin_raw, district_suffix_raw],
}

data_set = maker.get_data([locality])
```

Result

```
for row in data_set:
    data = Model()
    data.locality = row["locality"]
    data.save()
```

## `liar.raw.english`

Simply a list of about 6000 common English words. They are each catagorized by word type (Noun, Verb, Pronoun, etc) which helps to anticipate how the word might work in the sample data being created, for instance as something's name, or within a media title.

```
# Use:
from liar.model.raw import english_raw

# Or from scratch:
english_raw = {
    "name": "english_raw",
    "class": "igetraw",
    "data": "english",
}

data_set = maker.get_data([english_raw])
```

Result

```
for row in data_set:
    data = Model()
    english_word.word = row["english_raw"]["word"]
    english_word.classification = row["english_raw"]["classification"]
    data.save()
```

Blurb:

```
english_title = {
    "name": "english_title",
    "class": "iblurb",
    "language": "english",
    "method": "html_bullets",
    "min": 7,
    "max": 10,
}

data_set = maker.get_data([english_title])
```

Result

```
for row in data_set:
    data = Model()
    data.english_title = row["english_title"]
    data.save()
```

If you only need the english word, use the property option:

```
# Use:
from liar.model.raw import english_word_raw

# Or from scratch:
english_word_raw = {
    "name": "english_word_raw",
    "class": "igetraw",
    "data": "english",
    "property": "word"
}

number_records = 10
maker = IAmALiar(number_records)

data_set = maker.get_data([english_word_raw])
```

Result

```
for row in data_set:
    data.english_word = row["english_word_raw"]
```

## `liar.raw.geo_location`

A list of real cities, regions and countries and other data.

```
# Use:
from liar.model.raw import geo_location_raw

# Or from scratch:
geo_location_raw = {
    "name": "geo_location_raw",
    "class": "igetraw",
    "data": "geo_location",
}

data_set = maker.get_data([geo_location_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.city = row["geo_location_raw"]["city"]
    data.region = row["geo_location_raw"]["region"]
    data.country = row["geo_location_raw"]["country"]
    data.population = row["geo_location_raw"]["population"]
    data.iso2 = row["geo_location_raw"]["iso2"]
    data.iso3 = row["geo_location_raw"]["iso3"]
    data.lat = row["geo_location_raw"]["lat"]
    data.long = row["geo_location_raw"]["long"]
    data.flag = row["geo_location_raw"]["images"]["flag"]
    data.save()
```

## `liar.raw.job_title`

About 300 occupations which can be used when constructing information about people.

```
# Use:
from liar.model.raw import job_title_raw

# Or from scratch:
job_title_raw = {
    "name": "job_title_raw",
    "class": "igetraw",
    "data": "job_title",
}

data_set = maker.get_data([job_title_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.job_title = row["job_title_raw"]
    data.save()
```

## `liar.raw.latin`

Latin seems familiar to speakers of romantic langauges and they all seem similar to eachother. This makes the words great for misc. blurb and fake names of all types things. Traditionally Latin is used for this purpose. (Also see the Ainu class for a slightly different version.

```
# Use:
from liar.model.raw import latin_raw

# Or from scratch:
latin_raw = {
    "name": "latin_raw",
    "class": "igetraw",
    "data": "latin",
}

data_set = maker.get_data([latin_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.latin_word = row["latin_raw"]
    data.save()
```

Blurb:

```
latin_title = {
    "name": "latin_title",
    "class": "iblurb",
    "language": "latin",
    "method": "plaintext_title",
    "min": 5,
    "max": 5,
}

latin_motto = {
    "name": "latin_motto",
    "class": "iblurb",
    "language": "latin",
    "method": "plaintext_title",
    "min": 2,
    "max": 10,
}

data_set = maker.get_data([latin_title, latin_motto])
```

Result

```
for row in data_set:
    data = Model()
    latin_blurb = row["latin_title"]
    latin_motto = row["latin_motto"]
```

## `liar.raw.person`

About 5000 names, each with an indication of gender and therefore an appropriate Title (Mr, Mrs, Ms, etc).

```
# Use:
from liar.model.raw import person_raw

# Or from scratch:
person_raw = {
    "name": "person_raw",
    "class": "igetraw",
    "data": "person",
}

data_set = maker.get_data([person_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.title = row["person_raw"]["title"]
    data.first_name = row["person_raw"]["first_name"]
    data.sex = row["person_raw"]["sex"]
    data.save()
```

If you only need the first_name, use the property option:

```
# Use:
from liar.model.raw import first_name_raw

# Or from scratch:
first_name_raw = {
    "name": "first_name_raw",
    "class": "igetraw",
    "data": "person",
    "property": "first_name"
}

data_set = maker.get_data([first_name_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.first_name = row["first_name_raw"]
    data.save()
```

## `liar.raw.product`

Using data typical of supermarkets, superstores and online aunctions this collection is perfect for creating test data for ecommerce applications. It might also be useful for teachers in class room activities. Products are classified as Grocery, Superstore or Auction, and also contain a sub category (bathroom, computing, dry goods, etc). NB: I have also kept the orginal price in place to help with the realistic feel to the fake data, but it would be just as easy to use the Primitive data class to do this.

```
# Use:
from liar.model.raw import product_raw

# Or from scratch:
product_raw = {
    "name": "product_raw",
    "class": "igetraw",
    "data": "product",
}

data_set = maker.get_data([product_raw])
```

Result

```
for row in data_set:
    data = Model()
    product.product = row["product_raw"]["product"]
    product.classification = row["product_raw"]["classification"]
    product.category = row["product_raw"]["category"]
    product.price = row["product_raw"]["price"]
    data.save()
```

Create a definition which only gets the product_name, using the property option:

```
# Use:
from liar.model.raw import product_name_raw

# Or from scratch:
product_name_raw = {
    "name": "product_name_raw",
    "class": "igetraw",
    "data": "product",
    "property": "product",
}

data_set = maker.get_data([product_name_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.product_name = row["product_name_raw"]
    data.save()
```

## `liar.raw.street_type`

Words like Street, Road, Lane, etc, which can be neatly appended to words and phrases to create realistic address data.

```
# Use:
from liar.model.raw import street_type_raw

# Or from scratch:
street_type_raw = {
    "name": "street_type_raw",
    "class": "igetraw",
    "data": "street_type",
}

data_set = maker.get_data([street_type_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.subject = row["street_type_raw"]["subject"]
    data.department = row["street_type_raw"]["department"]
    data.save()
```

Create a definition which concatonates a latin word to a street_type to make a road name

```
# Use:
from liar.model.common import nb_space
from liar.model.int import in_100
from liar.model.raw import animal_name_raw, street_type_raw

address1 =   {
    "name": "address1",
    "class": "concat",
    "data": [
            in_100,
            nb_space,
            animal_name_raw,
            nb_space,
            street_type_raw,
        ]
}

data_set = maker.get_data([address1])
```

Result

```
for row in data_set:
    data = Model()
    data.locality = row["address1"]
    data.save()
```

## `liar.raw.surname`

A large list of surname, or family names, from around the world.

```
# Use:
from liar.model.raw import surname_raw

# Or from scratch:
surname_raw = {
    "name": "surname_raw",
    "class": "igetraw",
    "data": "surname",
}

data_set = maker.get_data([surname_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.subject = row["surname_raw"]
    data.save()
```

## `liar.raw.training_course`

A list real vehicle manufacturers.

```
# Use:
from liar.model.raw import transport_raw

# Or from scratch:
transport_raw = {
    "name": "academic_raw",
    "class": "igetraw",
    "data": "transport",
}

data_set = maker.get_data([transport_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.make = row["transport_raw"]["make"]
    data.class = row["transport_raw"]["class"]
    data.img = row["transport_raw"]["img"]
    data.save()
```

## `liar.raw.transport`

A list real vehicle manufacturers.

```
# Use:
from liar.model.raw import transport_raw

# Or from scratch:
transport_raw = {
    "name": "academic_raw",
    "class": "igetraw",
    "data": "transport",
}

data_set = maker.get_data([transport_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.make = row["transport_raw"]["make"]
    data.class = row["transport_raw"]["class"]
    data.img = row["transport_raw"]["img"]
    data.save()
```

## `liar.raw.word_prefix`

A list of 150 or so words that you'll typically find at the front of English words, like pre, en, in, sub, etc. You can use them to add a greater degree of randomness and flexibility when creating data with any of the dictionaries in this program.

```
# Use:
from liar.model.raw import word_prefix_raw

# Or from scratch:
word_prefix_raw = {
    "name": "word_prefix_raw",
    "class": "igetraw",
    "data": "word_prefix",
}

data_set = maker.get_data([word_prefix_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.subject = row["word_prefix_raw"]["subject"]
    data.department = row["word_prefix_raw"]["department"]
    data.save()
```

Make a compound word.

```
# Use:
from liar.model.raw import word_prefix_raw, english_word_raw, word_suffix_raw

compound_word =  {
    "name": "compound_word",
    "class": "concat",
    "data": [word_prefix_raw, english_word_raw, word_suffix_raw],
}

data_set = maker.get_data([compound_word])
```

Result

```
for row in data_set:
    data = Model()
    data.compound_word = row["compound_word"]
    data.save()
```

## `liar.raw.word_suffix`

A list of 150 or so words that you'll typically find at the end of English words, like ous, able, ster, ize, etc. You can use them to add a greater degree of randomness and flexibility when creating data with any of the dictionaries in this program.

```
# Use:
from liar.model.raw import word_suffix_raw

# Or from scratch:
word_suffix_raw = {
    "name": "word_suffix_raw",
    "class": "igetraw",
    "data": "word_suffix",
}

data_set = maker.get_data([word_suffix_raw])
```

Result

```
for row in data_set:
    data = Model()
    data.word_suffix = row["word_suffix_raw"]
    data.save()
```
