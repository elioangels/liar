# liar Quickstart

## Prerequisites

- liar Installed [No!](installing.md)

## Nutshell Checklist

To create some new liar data:

- Create a python file.

- Import `IAmALiar` and instantiate:

```python
from liar.iamaliar import IAmALiar
number_records = 10
maker = IAmALiar(number_records)
```

- Create a definition:

```python
datadef = [{
  "name": "tldr",
  "class": "quicklist",
  "data": ["T", "L", "D", "R"]
}]
```

... or use a ready-made definition.

```python
from liar.model.marketing import product_title, postage_cost
from liar.model.date import within_days

datadef = [product_title, postage_cost, within_days]
```

- Build and output the data.

```python
my_data_set = maker.get_data(datadef)
print(my_data_set)
```

## Exercise 1: Dicey

Create a new python file in the app where **liar** is installed. In this example we assume you are putting a file in the `elioangels/liar/examples` folder called `exercise1.py`.

Open `exercise1.py`.

- Import the IAmALiar module.

```python
from liar.iamaliar import IAmALiar
```

- Instantiate the class and set the required number of records.

```python
maker = IAmALiar(100)
```

- A dice is easy. We can use a simple primitive type:

```python
dice = {
"name": "dice",
"class": "iamprimitive",
"method": "int_list",
"min": 1,
"max": 6,
}
```

Or a quicklist:

```python
dice = {
  "name": "dice",
  "class": "quicklist",
  "data": [1, 2, 3, 4, 5, 6]
}
```

A dice is too easy, let's enable a game of backgammon and use two dice. The important thing is not to use the same field name. The utility method is great for reusing definitions and avoiding [Passing By Reference](https://8thlight.com/blog/shibani-mookerjee/2019/05/07/some-common-gotchas-in-python.html) gotchas.

- Import the `rename_def` utility at the top. If you're a big boy, you don't have to use it.

```python
from liar.ijusthelp import rename_def
```

- Base your two dice on the first definition, rewriting and copying it as the same time:

```python
dice1 = rename_def(dice, "dice1")
dice2 = rename_def(dice, "dice2")
```

- Now generate the data and print the data to the console:

```python
print(maker.get_data([dice1, dice2]))
```

- Finally save and run your script at the shell: `python examples/exercise1.py`. You should see the following print out.

```shell
[
  {'dice1': 2, 'dice2': 1},
  {'dice1': 5, 'dice2': 5},
  {'dice1': 6, 'dice2': 3},
  {'dice1': 5, 'dice2': 4}
]
```

We don't know who is rolling: Let's add the player of each dice, cycling between "white" and "black".

- Add a toothpaste definition.

```python
player = {
  "name": "player",
  "class": "toothpaste",
  "data": ["white", "black"],
}
```

- Now generate the data and print the data to the console:

```python
backgammon_model = [player, dice1, dice2]
print(maker.get_data(backgammon_model))
```

You should now see a printout like this:

```shell
[
  {"player": "white", "dice1": 4, "dice2": 1},
  {"player": "black", "dice1": 6, "dice2": 5},
  {"player": "white", "dice1": 6, "dice2": 6},
  {"player": "black", "dice1": 1, "dice2": 5},
  ...
]
```

## Exercise 2: People are tricky

Let's try something a bit more useful and put out some data we can use for a contact app.

- Create and open `exercise2.py` and instantiate `IAmALiar` at the top.

```python
from liar.iamaliar import IAmALiar
maker = IAmALiar(100)
```

Databases can usually provide a primary key by default, but often we'll want to create relatable data - and for that a primary key we can predict in our random data is useful.

- The pk class is the simplest type of data you can create. Add this to your code file:

```python
pk = { "name": "pk", "class": "pk" }
```

We need some names, and for that we'll use the raw data provided.

- The raw data comes with first names, genders and titles in one data set, and last names in another.

```python
person = { "name": "person", "class": "igetraw", "data": "person", }
surname = { "name": "surname", "class": "igetraw", "data": "surname", }
print(maker.get_data([pk, person, surname]))
```

Run this python script and you should You should see something like this:

```shell
[
    {
        "pk": 1,
        "person": {"title": "Mr", "first_name": "Felipe", "sex": "male"},
        "surname": "Miriam",
    },
    {
        "pk": 2,
        "person": {"title": "Miss", "first_name": "Ashlee", "sex": "female"},
        "surname": "Fitzhenry",
    },
]
```

Notice that the "person" field is a JSON object?

- If you'd prefer a flatter result, add the `flatten` option.

```python
person ={
    "name": "person",
    "class": "igetraw",
    "data": "person",
    "flatten": True
}
```

You should see something like this:

```shell
[
    {
        "pk": 1,
        "person_first_name": "Portia",
        "person_sex": "female",
        "person_title": "Mrs",
        "surname": "Sevearengen",
    },
    {
        "pk": 2,
        "person_first_name": "Johnathan",
        "person_sex": "male",
        "person_title": "Mr",
        "surname": "Franzmeier",
    },
]
```

Notice that the field name you chose is joined to the field name of the flattened columns. This is important because it means you can reuse the person raw data and add another person to your data set.

We will extend our data set to produce married couples. For this will use the `filters` option.

- Extend your data defintions so that the couples are male and female (just for demo purposes!).

```python
husband = {
    "name": "husband",
    "class": "igetraw",
    "data": "person",
    "flatten": True,
    "filters": {"sex": ["male"]},
}
wife = {
    "name": "wife",
    "class": "igetraw",
    "data": "person",
    "flatten": True,
    "filters": {"sex": ["female"], "title": ["Mrs", "Dr"]},
}
surname = { "name": "surname", "class": "igetraw", "data": "surname" }
print(maker.get_data([pk, husband, wife, surname]))
```

- Now our data set has two persons per record, each with unique field names:

```shell
[
    {
        "pk": 1,
        "husband_first_name": "Kerry",
        "husband_sex": "male",
        "husband_title": "Mr",
        "surname": "Smith",
        "wife_first_name": "Maude",
        "wife_sex": "female",
        "wife_title": "Mrs",
    },
    {
        "pk": 2,
        "husband_first_name": "Eusebio",
        "husband_sex": "male",
        "husband_title": "Mr",
        "surname": "Jones",
        "wife_first_name": "Victoria",
        "wife_sex": "female",
        "wife_title": "Dr",
    },
]
```

Finally we will add an email address each.

- Import the `rewrite_dict` helper and a prepared defintion of an email address called `personal_email_domains`.

```python
from liar.ijusthelp import rewrite_dict
from liar.model.personal import personal_email_domains
```

- Add a `field` class to access the husband's first name in lower case.

```python
husband_user_name = {
    "name": "husband_user_name",
    "class": "field",
    "data": "husband_first_name",
    "remove": True,
}
```

Because we don't want this field in our results, we flag to have it removed from results using the `remove` flag.

- Use this as a base to create a username for the wife:

```python
wife_user_name = rewrite_dict(
    husband_user_name, {"name": "wife_user_name", "data": "wife_first_name"}
)
```

- Define an email address for the husband and wife, using the `concat` class to build compound data.

NB: using the `slugify` transformation to ensure we remove spaces, etc

```python
husband_email = {
    "name": "husband_email",
    "class": "concat",
    "data": [
        husband_user_name,
        {"class": "quicklist", "data": ["_", ".", "-", ""]},
        {"class": "field", "data": "surname"},
        {"class": "exact", "data": "@"},
        personal_email_domains,
    ],
    "itransform": ["slugify"],
}
wife_email = {
    "name": "wife_email",
    "class": "concat",
    "data": [
        wife_user_name,
        {"class": "quicklist", "data": ["_", ".", "-", ""]},
        {"class": "field", "data": "surname"},
        {"class": "exact", "data": "@"},
        personal_email_domains,
    ],
    "itransform": ["slugify"],
}
```

Notice we are using the imported `personal_email_domains` definition provide some random email domain names.

- Put the definitions together, making use that you put the fields in the order they are called by the `field` class:

```python
married_couple_model = [
  pk,
  husband,
  wife,
  surname,
  husband_user_name,
  wife_user_name,
  husband_email,
  wife_email
]
print(maker.get_data(married_couple_model))
```

And you should see something like this. Notice that the `husband_user_name` and `wife_user_name` fields are not included.

```
[
    {
        "husband_email": "branden-perfect@gmail.co.uk",
        "husband_first_name": "Branden",
        "husband_sex": "male",
        "husband_title": "Mr",
        "pk": 1,
        "surname": "Perfect",
        "wife_email": "athena_perfect@yahoo.net",
        "wife_first_name": "Athena",
        "wife_sex": "female",
        "wife_title": "Mrs",
    },
    {
        "husband_email": "cornelius.neelaenbraun@yahoo.com",
        "husband_first_name": "Cornelius",
        "husband_sex": "male",
        "husband_title": "Mr",
        "pk": 2,
        "surname": "Neelaenbraun",
        "wife_email": "elda-neelaenbraun@live.co.uk",
        "wife_first_name": "Elda",
        "wife_sex": "female",
        "wife_title": "Mrs",
    },
]
```

## Exercise 3: Down to business

This exercise demonstrates more complex data definitions.

- Create and open `exercise3.py`

- Instantiate `IAmALiar` at the top.

```python
from liar.iamaliar import IAmALiar
maker = IAmALiar(10)
```

- First add a business name. For this we will use the "danish" library and combine it with some company suffixes using the `concat` class:

```shell
company_name = {
    "name": "company_name",
    "class": "concat",
    "data": [
        {
            "class": "iblurb",
            "method": "plaintext_title",
            "min": 1,
            "max": 2,
            "language": "danish",
            "itransform": ["title"]
        },
        {
            "class": "quicklist",
            "data": [" Ltd", " Pty", " Corporation", " Plc", " Limited", " & Sons"],
        },
    ],
}
```

Notice how the `concat` class contains two sub defintions. NB: Sub definitions need no name.

In `IAmALiar` you can combine lots of definitions into 1 field this way - even nesting more `concat` classes.

As well as a simple `quicklist` we have also used the `iblurb` to build a company name from one or two Danish words - which should sound pompous enough. Adding the `itransform` option to "title" case.

- Add a `pk` and show the records.

```python
from liar.model.common import pk
pp.pprint(maker.get_data([pk, company_name]))
```

Your output will look something like this:

```shell
[
  {'company_name': 'Andres & Sons', 'pk': 1},
  {'company_name': 'Opdager Pty', 'pk': 2},
  {'company_name': 'Præsident Plc', 'pk': 3},
  {'company_name': 'Spøgelser Gãr Corporation', 'pk': 4},
]
```

Now we will add a couple of fields suitable for a CMS: company size and company worth.

- We start with the company size:

```python
company_size = {
    "name": "company_size",
    "class": "primitive",
    "method": "int_list",
    "min": 5,
    "max": 500,
}
```

Now we will add company worth. For this field we will base it on the company size, ensuring the company's value is logical. For this we use the `field` class and refer to the field we created above. We've also added a `calc` option to alter, then format, the value of the "company_size".

- This value is concatonated to a "\$" sign for good measure.

```python
company_worth = {
    "name": "company_worth",
    "class": "concat",
    "data": [
        {"class": "exact", "data": "$ "},
        {
            "class": "field",
            "data": "company_size",
            "calc": [{"multiply": 10000}, {"divide": 1.1}, {"format": "{:,.2f}"}],
        },
    ],
}
company_prospect_model = [pk, company_name, company_size, company_worth]
print(maker.get_data(company_prospect_model))
```

NB: It is important the company_size field comes before the company_worth field when you call the `get_data` method.

You should see something like this:

```shell
[
    {
        "company_name": "Røven Limited",
        "company_size": 14,
        "company_worth": "$ 127,272.73",
        "pk": 1,
    },
    {
        "company_name": "Smutter Corporation",
        "company_size": 146,
        "company_worth": "$ 1,327,272.73",
        "pk": 2,
    },
    {
        "company_name": "Bekræftet Plc",
        "company_size": 491,
        "company_worth": "$ 4,463,636.36",
        "pk": 3,
    },
]
```

## Next

This quickstart has briefly introduced 3 scenarios where fake data might be useful, and we have covered about 90% of **liar**'s features. To see the full list of features, we suggest refering to the API.

- [API documentation](api.html)
