# liar Credits

## Artwork

- [wikimedia:Bocca_della_Verità](<https://commons.wikimedia.org/wiki/File:Lucas_Cranach_d.Ä._-_Bocca_della_Verità_(ca.1530).jpg>)
- [wikimedia:Sewage_Outfall](https://commons.wikimedia.org/wiki/File:Sewage_Outfall_-_geograph.org.uk_-_410000.jpg)
- [wikimedia:LEGO*City_10159_LEGO_City_Airport*](<https://commons.wikimedia.org/wiki/File:LEGO_City_10159_LEGO_City_Airport_(29018510848).jpg>)
- [wikipedia:Isleworth_Mona_Lisa](https://en.wikipedia.org/wiki/Isleworth_Mona_Lisa)
- <https://unsplash.com/photos/ehdI_89nzMo>
- <https://commons.wikimedia.org/wiki/File:Still_life_-_Caravaggio.png>

## These were useful

- <https://packaging.python.org/tutorials/packaging-projects/>
- <https://truveris.github.io/articles/configuring-pypirc/>
