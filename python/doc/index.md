# liar

<figure>
  <img src="star.png" alt="">
</figure>

> Fake it until you make it **the elioWay**

![release](/artwork/icon/release/favicon.png "release")

A source of random but realistic looking data. This app works by combining randomly generated primitive data and prepared realistic data to create realistic random records that you can use to test and demo your databases and apps.

The main tool is `IAmALiar`, which is initialised for a certain number of records, then passed definitions of a data model - the instructions for randomly building data into columns. **liar** provides a set of tools to quickly spin up realistic looking sample data.

It's not like there aren't already plenty of random data generators for such things. In **liar** we focus on generating consistent data - which provides whole records of related data - the email address matching the name of a thing - the town matching the country - the animal name matching the banjo - and so on. When using fake data to develop applications, it's important to get an early warning when the integrity of your application is compromised. How can fake data do that unless the integrity of it is sound first?

<div><a href="installing.html">
  <button>Installing</button>
</a>
  <a href="quickstart.html">
  <button>Quickstart</button>
</a></div>

## Cult of Lies

**liar**'s in-depth documentation.

<section>
  <figure>
  <img src="/artwork/icon/api/artwork/splash.jpg">
  <h3>Book of Lies</h3>
  <p>API Documentation for building fake data.</p>
  <p><a href="api.html"><button><img src="/artwork/icon/api/apple-touch-icon.png">API</button></a></p>
</figure>
  <figure>
  <img src="/artwork/icon/model/artwork/splash.jpg">
  <h3>Building Models</h3>
  <p>Ready-to-use data models and common fields.</p>
  <p><a href="model.html"><button><img src="/artwork/icon/model/apple-touch-icon.png">Models</button></a></p>
</figure>
  <figure>
  <img src="/artwork/icon/raw/artwork/splash.jpg">
  <h3>Getting Raw</h3>
  <p>Realistic, raw and useable fake data.</p>
  <p><a href="raw.html"><button><img src="/artwork/icon/raw/apple-touch-icon.png">Raw</button></a></p>
</figure>
</section>

### Full Documentation

- [Installing](installing.html)
- [Quickstart](quickstart.html)
- [API](tags.html)
- [Models](model.html)
- [Raw](raw.html)

## Author's Note

**liar** was originally called **INeedData** and has been moved here and renamed (the elioWay) from my personal repo. This was one of the author's first python applications and was a great way to learn the basics of this programming language. For new python programmers I recommend a project like this which presents challenges which can all be solved using standard pythonic approaches like lists, dictionaries, tuples, classes, functions, all the data types, and plenty of one liners.
