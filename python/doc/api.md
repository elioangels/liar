# API Documentation

## Defining Models and Data Definitions

Build on the defintion library by writing your own fields and models. Each column/field in your dataset is defined by a dictionary in a list.

```
model_def = [
  { define_field1 },
  { define_field2 },
  { define_field3 },
  { define_field4 },
]
```

Each defined field needs a name and a class.

`name` will be the field name used in the results. It should be unique.

```
define_field1 = {
  "name": "animal_name",
  "class": "quicklist",
  "data": ["dog", "cat", "mouse", "horse", "rat"]
},
```

The class is the programming class where the data is taken from. There are **eleven types of class**:

## `pk` class

A unique number for each record.

```
contact_id = {
  "name": "contact_id",
  "class": "pk",
}
```

Result

```
[
  {"contact_id": 1},
  {"contact_id": 2},
  {"contact_id": 3},
  {"contact_id": 4},
]
```

## `quicklist` class

A user supplied list of choices for each field.

```
in_the_list = {
  "name": "in_the_list",
  "class": "quicklist",
  "data": [1, "of", "the", "items", "in", "this", "list"]
}
```

Result

```
[
  {"in_the_list": "of"},
  {"in_the_list": "in"},
  {"in_the_list": 1},
  {"in_the_list": "items"},
]
```

## `toothpaste` class

A list we rotate/cycle through.

```
not_grey = {
  "name": "not_grey",
  "class": "toothpaste",
  "data": ["black", "white"]
}
```

Result

```
[
  {"not_grey": "black"},
  {"not_grey": "white},
  {"not_grey": "black"},
  {"not_grey": "white},
  {"not_grey": "black"},
  {"not_grey": "white},
]
```

## `exact` class

A column of static data. This is very useful as a defintion in the `concat` class, for instance using an "@" symbol to build an email address.

```
tax_code = {
  "name": "tax_code",
  "class": "exact",
  "data": "A",
}
```

Result

```
[
  {"tax_code": "A"},
  {"tax_code": "A"},
  {"tax_code": "A"},
  {"tax_code": "A"},
]
```

### Options

#### `data`

The exact string you want included in the data.

## `igetraw` class

**liar** comes with json formatted files containing realistic looking data (see raw folder). You can add your own. For those raw files whose data is made of dictionary objects in a list, it will assign the whole dictionary to the column:

```
study_area = {
  "name": "study_area",
  "class": "igetraw",
  "data": "academic"
}
```

Result

```
[
  {"study_area": {"subject": "Philosophy", "department": "Arts"}},
  {"study_area": {"subject": "Computing", "department": "Science"}},
  {"study_area": {"subject": "Accounting", "department": "Business"}},
  {"study_area": {"subject": "Physics", "department": "Science"}},
]
```

### Options

#### `property`

Specify one of properties of the dictionary object to return a single column even if the source contains a dictionary.

```
{
  "property": "department",
}
```

Result

```
[
  {"study_area": "Arts"},
  {"study_area": "Science"},
  {"study_area": "Business"},
  {"study_area": "Science"},
]
```

#### `filters`

Filter the results by any property.

```
{
  "filters": {
      "department": ["Arts"],
      "subject": ["Art", "Art History", "Drama"],
      ...more filters...
    }
}
```

Result

```
[
  {"study_area": "Arts"},
  {"study_area": "Science"},
  {"study_area": "Science"},
  {"study_area": "Arts"},
]
```

#### `flatten`

Flatten into separate columns, any results for raw data which returns the field as a dictionary.

```
study_area = {
  "name": "study_area",
  "class": "igetraw",
  "data": "academic"
}
```

Result

```
# Instead of:
[
  {"study_area": {"subject": "Philosophy", "department": "Arts"}},
  # etc ...

# Returns:
[
  {"study_area_subject": "Philosophy", "study_area_department": "Arts"},
  {"study_area_subject": "Computing", "study_area_department": "Science"},
  {"study_area_subject": "Accounting", "study_area_department": "Business"},
  {"study_area_subject": "Physics", "study_area_department": "Science"},
  # etc ...
```

## `iamprimitive` class

Make columns of numbers, dates, times, decimals, primary keys. min and max sets a limit and takes any date, integer or decimal, e.g. "1940-01-01", 1, 0.23; defaults min:1, max:2

```
product_weight =  {
  "name": "product_weight",
  "class": "iamprimitive",
  "method": "dec_list",
  "min": 1.4,
  "max": 2.8,
}
```

Result

```
[
  {"product_weight": 2.4},
  {"product_weight": 2.1},
  {"product_weight": 1.4},
  {"product_weight": 2.0},
]
```

### Options

#### `method`

The data type to select from the `IAmPrimitive` class. Options are:

- `dec_list`
- `int_list`
- `datetimes_list`
- `time_list`
- `date_list`
- `primary_key_list`
- `file_list`

#### `min`/`max`

Where applicable, sets a `min` and `max` value for the random primitive.

#### `path`

A folder to extract files from recursively.

#### `file_types`

A list of file types to extract.

## `field` class

The field class clones from data already made in another field. Perfect to use in `concat` definitions to make email addresses and URLs which match data other fields; or when the relationship helps make the data look more realistic. In the example below we price the postage cost in line with the "product_weight" field above. **The field name being referenced must come first in the model definition.**

```
# See above for product_weight

postage_cost = {
  "name": "postage_cost",
  "class": "field",
  "data": "product_weight",
  "calc": [{"multiply": 10}, {"divide": 2}, {"add": 0.99}]
}
```

Result

```
[
  {"postage_cost": 12.99},
  {"postage_cost": 11.49},
  {"postage_cost": 7.99},
  {"postage_cost": 10.99},
]
```

### Options

#### `data`

The name of the field you are referencing.

## `iblurb` class

Make words, titles, sentences, paragraphs, html, etc. method can be any of the methods in the IMakeBlurb class. `min` and `max` represents the number words which the blurb will contain.

```
dvd_title = {
  "name": "dvd_title",
  "class": "iblurb",
  "method": "plaintext_title",
  "min": 3,
  "max": 5,
  "language": "ainu",
}
```

Result

```
[
  {"dvd_title": "Erum noye upaskuma eani"},
  {"dvd_title": "Orwa toyko pewrekur"},
  {"dvd_title": "Nuye reekoh cipo siyeye seta"},
  {"dvd_title": "Anekuroro yupi keran pirkaike"},
]
```

### Options

#### `method`

What kind of blurb you need. Valid methods are:

- `plaintext_phrase` A plaintext phrase with no punctuation.
- `plaintext_title` A plaintext phrase starting with a capital letter.
- `plaintext_sentence` A plaintext sentence starting with a capital letter and ending with full stop.
- `plaintext_sentences` Plaintext sentences starting with a capital letter and ending with full stop.
- `plaintext_bullets` A plaintext bullet list.
- `plaintext_paragraphs` A plaintext paragraphs list.
- `html_paragraph` An HTML rendered paragraph.
- `html_bullets` HTML rendered bullets.
- `html_paragraphs` HTML rendered paragraphs.
- `html` HTML rendered paragraphs and bullets.

#### `min`/`max`

Sets the min and max numberof words for the blurb.

#### `language`

Language can be any of raw json files which contain a word property. They are:

- english
- latin
- ainu
- danish

Add your own!

## `concat` class

Combine a nested list of definitions, and the results from these fields will be concatonated into the single field. They can be defined like any other field, and can include another `concat` class. They need no name. **Results are concatonated with no spaces; make your own spaces.**

```
what_i_like = {
  "name": "what_i_like",
  "class": "concat",
  "data": [
    {
    "class": "exact",
    "data": "I like ",
    },
    {
    "class": "quicklist",
    "data": ["sports", "music", "dancing", "reading"],
    }
   ],
}
```

Result

```
[
  {"what_i_like": "I like music"},
  {"what_i_like": "I like music"},
  {"what_i_like": "I like reading"},
  {"what_i_like": "I like sports"},
]
```

## `choose` class

Use this class to choose from a sub list of definitions; and only select one randomly per record. The sub list can be any of the available definitions nested as deeply as you like.

```
color_with_number = {
  "name": "color_with_number",
  "class": "choose",
  "data": [
        {
          "class": "concat",
          "data": [
                    {
                        "class": "exact",
                        "data": "Red",
                    },
                    {
                        "class": "iamprimitive",
                        "method": "int_list",
                        "min": 2,
                        "max": 5,
                    },
                  ]
        },
        {
          "class": "concat",
          "data": [
                    {
                        "class": "exact",
                        "data": "Green",
                    },
                    {
                        "class": "iamprimitive",
                        "method": "int_list",
                        "min": 20,
                        "max": 50,
                    },
                  ]
        },
    ],
}
```

Result

```
[
  {"color_with_number": "Red2"},
  {"color_with_number": "Green35"},
  {"color_with_number": "Green42"},
  {"color_with_number": "Red4"},
]
```

## `iwalk` class

To add a list of binary files to your data set (pdfs, images, etc).

```
gallery_image = {
  "name": "gallery_image",
  "class": "iwalk",
  "path": "/home/me/Pictures",
  "types": ["jpeg", "jpg", "png"],
}
```

Result

```
[
  {"gallery_image": "/home/me/Pictures/walk in park/alice on the swings.jpg"},
  {"gallery_image": "/home/me/Pictures/birthday/candles out.jpg"},
  {"gallery_image": "/home/me/Pictures/birthday/opening presents 1.jpg"},
  {"gallery_image": "/home/me/Pictures/walk in park/dog swimming.png"},
]
```

## Optional Properties

### `itransform`

A list of transformation methods from the ITransformData.Field class to post-process the data. Multiple transforms can be listed which are processed left to right.

```
{
  "itransform": [
            "upper",
            "lower",
            "chomp",
            "capitalize",
            "slugify",
            "title",
            ],
}
```

### `calc`

A list of calculations to perform on a field using any methods from the IAmPrimitiveLists.Calc class. calculations on dates are for hours. Multiple calculations can be listed and are performed left to right. `format` takes a standard python formatting string as a value.

```
{
  "calc": [{"add": 2}, {"subtract": 5}, {"multiply": 10}, {"divide": 3}, {"format": "{:2d}"}, ]
}
```

### `splutter`

By default `IAmALiar` fills 100% of fields in a column with data. Use `splutter` to make a percentage of fields (on average) blank. i.e. 10 = 10%~ of fields blank.

```
{
  "splutter": 10,
}
```

### `remove`

Use this option to remove columns/fields after the data is built - e.g. if they were temporary for making data available to concat class fields - e.g. a domain_name used for building email addresses and urls - but not needed as a column in the results.

```
{
  "remove": True,
}
```

## Next

- [Using Ready-Made Data Definitions and Models](model.html)
