![](https://elioway.gitlab.io/elioangels/liar/elio-liar-logo.png)

> Fake data, **the elioWay**

# liar ![alpha](/artwork/icon/alpha/favicon.png "alpha")

A source of random but realistic looking data the elioWay.

The main tool is `IAmALiar`, which is initialised for a certain number of records, then passed definitions of a data model - the instructions for randomly building data into columns.

- [liar Documentation](https://elioway.gitlab.io/elioangels/liar)

## Installing

```shell
pip install elio-liar
```

- [Installing liar](https://elioway.gitlab.io/elioangels/liar/installing.html)

## Seeing is Believing

```shell
git clone https://gitlab.com/elioangels/liar.git
virtualenv --python=python3 venv-liar
source venv-liar/bin/activate
# or
source venv-liar/bin/activate.fish
pip install -r requirements/local.txt
python examples/harry_potter.py
```

## Nutshell

### `python examples/harry_potter.py`

### `py.test -x`

- [liar Quickstart](https://elioway.gitlab.io/elioangels/liar/quickstart.html)
- [liar Credits](https://elioway.gitlab.io/elioangels/liar/credits.html)

![](https://elioway.gitlab.io/elioangels/liar/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)
