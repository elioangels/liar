import pprint
from liar.iamaliar import IAmALiar
from liar.ijusthelp import rewrite_dict

pp = pprint.PrettyPrinter(indent=4)

maker = IAmALiar(50)

pp.pprint(
    maker.get_data(
        [
            {
                "name": "spell",
                "class": "concat",
                "data": [
                    {
                        "class": "iblurb",
                        "method": "plaintext_title",
                        "min": 1,
                        "max": 3,
                        "language": "latin",
                        "itransform": ["title"],
                    },
                    {"class": "exact", "data": "!"},
                ],
            }
        ]
    )
)
