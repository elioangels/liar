import pprint
from liar.iamaliar import IAmALiar
from liar.ijusthelp import rewrite_dict

from liar.model.personal import personal_email_domains

pp = pprint.PrettyPrinter(indent=4)

maker = IAmALiar(10)

pk = {"name": "pk", "class": "pk"}

# A person defintion filtered to males.
husband = {
    "name": "husband",
    "class": "igetraw",
    "data": "person",
    "flatten": True,
    "filters": {"sex": ["male"]},
}

# A person defintion filtered to married female titles.
wife = {
    "name": "wife",
    "class": "igetraw",
    "data": "person",
    "flatten": True,
    "filters": {"sex": ["female"], "title": ["Mrs", "Dr"]},
}

surname = {"name": "surname", "class": "igetraw", "data": "surname"}

# A user name definition using the husband's first name field.
husband_user_name = {
    "name": "husband_user_name",
    "class": "field",
    "data": "husband_first_name",
    "itransform": ["lower"],
    "remove": True,
}


# Copy the `husband_user_name` defintion and change it for the wife.
wife_user_name = rewrite_dict(
    husband_user_name, {"name": "wife_user_name", "data": "wife_first_name"}
)

# Define an email address for the husband and wife, using the `concat` class to build compound data.
husband_email = {
    "name": "husband_email",
    "class": "concat",
    "data": [
        husband_user_name,
        {"class": "quicklist", "data": ["_", ".", "-", ""]},
        {"class": "field", "data": "surname"},
        {"class": "exact", "data": "@"},
        personal_email_domains,
    ],
    "itransform": ["lower"],
}
wife_email = {
    "name": "wife_email",
    "class": "concat",
    "data": [
        wife_user_name,
        {"class": "quicklist", "data": ["_", ".", "-", ""]},
        {"class": "field", "data": "surname"},
        {"class": "exact", "data": "@"},
        personal_email_domains,
    ],
    "itransform": ["lower"],
}

married_couple_model = [
    pk,
    husband,
    wife,
    surname,
    husband_user_name,
    wife_user_name,
    husband_email,
    wife_email,
]
pp.pprint(maker.get_data(married_couple_model))
