import pprint
from liar.iamaliar import IAmALiar
from liar.ijusthelp import rename_def

pp = pprint.PrettyPrinter(indent=4)

maker = IAmALiar(10)

# Make a defintion for random numbers between 1 and 6.
dice = {
    "name": "dice",
    "class": "iamprimitive",
    "method": "int_list",
    "min": 1,
    "max": 6,
}


# Make a defintion for two dice using the defintion above,
dice1 = rename_def(dice, "dice1")
dice2 = rename_def(dice, "dice1")

# Define a player.
player = {"name": "player", "class": "toothpaste", "data": ["white", "black"]}

backgammon_model = [player, dice1, dice2]
pp.pprint(maker.get_data(backgammon_model))
