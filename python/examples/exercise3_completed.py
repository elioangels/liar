import pprint
from liar.iamaliar import IAmALiar
from liar.ijusthelp import rewrite_dict

pp = pprint.PrettyPrinter(indent=4)

maker = IAmALiar(10)

pk = {"name": "pk", "class": "pk"}
company_name = {
    "name": "company_name",
    "class": "concat",
    "data": [
        {
            "class": "iblurb",
            "method": "plaintext_title",
            "min": 1,
            "max": 2,
            "language": "danish",
            "itransform": ["title"],
        },
        {
            "class": "quicklist",
            "data": [
                " Ltd",
                " Pty",
                " Corporation",
                " Plc",
                " Limited",
                " & Sons",
            ],
        },
    ],
}
company_size = {
    "name": "company_size",
    "class": "iamprimitive",
    "method": "int_list",
    "min": 5,
    "max": 500,
}
company_worth = {
    "name": "company_worth",
    "class": "concat",
    "data": [
        {"class": "exact", "data": "$ "},
        {
            "class": "field",
            "data": "company_size",
            "calc": [
                {"multiply": 10000},
                {"divide": 1.1},
                {"format": "{:,.2f}"},
            ],
        },
    ],
}

company_prospect_model = [pk, company_name, company_size, company_worth]
pp.pprint(maker.get_data(company_prospect_model))
