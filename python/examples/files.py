import pprint
from liar.iamaliar import IAmALiar
from liar.ijusthelp import rewrite_dict

pp = pprint.PrettyPrinter(indent=4)

maker = IAmALiar(50)

pp.pprint(
    maker.get_data(
        [
            {
                "name": "image",
                "class": "iwalk",
                "path": "/home/tim/Pictures",
                "types": [".jpg"],
            }
        ]
    )
)
