# -*- encoding: utf-8 -*-
""""""
import os
from liar.itransform import ITransform


class IWalk(object):
    """IWalk pulls path names to files in your folder system."""

    def walk(path, dataset_size, file_types=[]):
        """Pulls data files from a folder."""
        data = []
        for root, subs, files in os.walk(path):
            for file in files:
                file_path = os.path.join(root, file)
                if os.path.splitext(file_path)[1] in file_types or not len(
                    file_types
                ):
                    data.append(file_path)
        # Increase the size by doubling the set if there weren't enough records
        # in the raw data.
        if not len(data):
            raise Exception("Your iwalk definition found no files.")
        while len(data) < dataset_size:
            data += data
        return ITransform.Data.rand_sort(data)[:dataset_size]
